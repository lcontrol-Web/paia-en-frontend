export const environment = {
  production: true,
  apiUrl: 'http://localhost:3000',
  socketUrl: 'localhost:3000'
};
