import { DataService } from './../../../core/services/data.service';
import { ToasterService } from './../../../core/services/toaster.service';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-delete-form-modal',
  templateUrl: './delete-form-modal.component.html',
  styleUrls: ['./delete-form-modal.component.css']
})
export class DeleteFormModalComponent implements OnInit, OnDestroy {

  @Input() public items: any[] = [];
  @Input() public type: string;
  @Input() public description: string;
  @Input() public key: string;
  @Input() public serialNumber: string;
  @Output('onClose') private closeEmitter: EventEmitter<any> = new EventEmitter();
  @Output('onDelete') private deleteEmitter: EventEmitter<any[]> = new EventEmitter();

  public isLoading: boolean = false;

  constructor(private toasterService: ToasterService,
    private dataService: DataService) {
  }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#deleteModal').modal('show');
  }

  private closeModal() {
    $('#deleteModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private getIds(): string[] {
    return this.items.map(item => item[this.key]);
  }

  public onSubmit() {
    this.isLoading = true;
    this.dataService.apiName = this.type;
    // this.dataService.delete(this.getIds())
    //   .subscribe(
    //     (response: string[]) => {
    //       this.closeModal();
    //       setTimeout(() => {
    //         this.deleteEmitter.emit(this.items);
    //       }, 200);
    //     },
    //     (error: string) => {
    //       this.isLoading = false;
    //       this.toasterService.showError('Cannot delete item.');
    //     });

  }
}