import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-items-page-amount',
  templateUrl: './items-page-amount.component.html',
  styleUrls: ['./items-page-amount.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemsPageAmountComponent implements OnInit {

  @Input() public total: number = 0;
  @Input() public itemsPerPage: number = 10;
  @Input() public page: number = 1;
  @Output('onItemsPerPageChanged') private itemsPerPageChangedEmitter: EventEmitter<number> = new EventEmitter();

  public itemsPerPageOptions: any[] = [10, 25, 50, 100];

  constructor() { }

  ngOnInit() { }

  public itemsPerPageChanged() {
    this.itemsPerPageChangedEmitter.emit(+this.itemsPerPage);
  }
}
