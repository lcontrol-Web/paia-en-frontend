import { FilterTableService } from './../../../core/services/filter-table.service';
import { Component, EventEmitter, OnInit, Output, OnDestroy, Input } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FilterActionTypes } from 'src/app/core/enums/filter-action-types.enum';
import { FilterTableMessage } from 'src/app/core/models/filter-table-message';

@Component({
  selector: 'app-search-table',
  templateUrl: './search-table.component.html',
  styleUrls: ['./search-table.component.css']
})
export class SearchTableComponent implements OnInit, OnDestroy {

  @Output('onStartSearch') private startSearchEmitter: EventEmitter<any> = new EventEmitter();
  @Output('onEndSearch') private endSearchEmitter: EventEmitter<any> = new EventEmitter();
  @Output('onSearch') private searchEmitter: EventEmitter<string> = new EventEmitter();

  public searchValue: string;

  private searchTextChanged = new Subject<string>();
  private searchSubscription: Subscription;
  private filterTableServerSubscription: Subscription;

  constructor(private filterTableService: FilterTableService) {
    this.subscribeToSearch();
    this.subscribeToFilterTableServer();
  }

  ngOnInit() { }

  ngOnDestroy() {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }

    if (this.filterTableServerSubscription) {
      this.filterTableServerSubscription.unsubscribe();
    }
  }

  private subscribeToSearch() {
    this.searchSubscription = this.searchTextChanged.pipe(
      debounceTime(1000),
      distinctUntilChanged((prev, curr) => {

        if (prev === curr) {
          this.endSearchEmitter.emit();
          return true;
        }
        return false;
      }))
      .subscribe((data) => {

        this.filterTableService.onSearchTableFilter();
        this.searchEmitter.emit(this.searchValue);
      });
  }

  private subscribeToFilterTableServer() {
    this.filterTableServerSubscription = this.filterTableService.getMessage()
      .subscribe((message: FilterTableMessage) => {
        switch (message.action) {
          case FilterActionTypes.SortByColumn:
          case FilterActionTypes.FilterByColumn:
            this.clearSearch();
            break;
          default:
            break;
        }
      });
  }

  public searchValueChanged(event) {

    this.startSearchEmitter.emit();
    this.searchTextChanged.next(this.searchValue);
  }

  public clearSearch() {
    this.searchValue = '';

    // reset subscription
    this.searchSubscription.unsubscribe();
    this.subscribeToSearch();
  }
}
