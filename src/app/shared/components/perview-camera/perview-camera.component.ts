import { Component, Input, OnInit, EventEmitter, AfterViewInit } from '@angular/core';
import { Output } from '@angular/core';
import { VideoPerviewStatus } from 'src/app/core/enums/video-perview-status.enum';
import { VideoPreviewMessage } from 'src/app/core/models/video-preview-message';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { Camera } from 'src/app/modules/cameras/models/camera';
import { Nvr } from 'src/app/modules/nvrs/models/nvr';
declare var WebVideoCtrl: any;
declare var $: any;

@Component({
  selector: 'app-perview-camera',
  templateUrl: './perview-camera.component.html',
  styleUrls: ['./perview-camera.component.css']
})
export class PerviewCameraComponent implements OnInit, AfterViewInit {

  @Output() onWindowChanged = new EventEmitter<number>();
  @Input() height: number;
  @Input() width: number;
  @Input() fullWidth: boolean = false;
  @Input() amountOfScreens: number;
  @Input() id: string;

  public g_iWndIndex: number = 0;
  public pluginInstalled: boolean = false;

  constructor(private toasterService: ToasterService) { }

  ngOnInit() {
    this.check();
  }

  ngAfterViewInit(): void {
    if (this.fullWidth)
      $('object').css('width', '100%');
    else
      $('object').css('width', this.width + 'px');

    $('object').css({ 'height': this.height + 'vh' });
  }

  private check() {
    
    // check the installation status of plugin 
    if (-1 == WebVideoCtrl.I_CheckPluginInstall()) {
      //alert(" If the plugin is uninstalled, please install the WebComponents.exe!");
      return;
    }
    
    this.pluginInstalled = true;

    // Init plugin parameters and insert the plugin
    WebVideoCtrl.I_InitPlugin(0, 0, {
      bWndFull: true,
      iWndowType: this.amountOfScreens,
      cbSelWnd: (xmlDoc) => {
        this.g_iWndIndex = $(xmlDoc).find('SelectWnd').eq(0).text();
        this.onWindowChanged.emit(this.g_iWndIndex);
      },
      cbDoubleClickWnd: (iWndIndex, bFullScreen) => {
        var szInfo = 'present window number to zoom: ' + iWndIndex;
        if (!bFullScreen)
          szInfo = 'present window number to restore: ' + iWndIndex;


        // you can handle the single window bit stream switching here
        /*if (bFullScreen) {
            clickStartRealPlay(1);
        } else {
            clickStartRealPlay(2);
        }*/
      },
      cbEvent: (iEventType, iParam1, iParam2) => {
        console.log(iEventType);
        if (2 == iEventType) { // playback finished normally

        } else if (-1 == iEventType) {

        } else if (3001 == iEventType) {

        }
      },
      cbInitPluginComplete: () => {

        WebVideoCtrl.I_InsertOBJECTPlugin('divPlugin');

        // check plugin to see whether it is the latest
        if (-1 == WebVideoCtrl.I_CheckPluginVersion()) {
          alert('Detect the latest version, please double click WebComponentsKit.exe to update!');
          return;
        }
      }
    });

    // window event binding
    $(window).bind({
      resize: () => {
        var $Restart = $('#restartDiv');
        if ($Restart.length > 0) {
          var oSize = this.getWindowSize();
          $Restart.css({
            width: oSize.width + 'px',
            height: oSize.height + 'px'
          });
        }
      }
    });

    // // init date
    // var szCurTime = this.dateFormat(new Date(), "yyyy-MM-dd");
    // $("#starttime").val(szCurTime + " 00:00:00");
    // $("#endtime").val(szCurTime + " 23:59:59");
  }

  // get window size
  private getWindowSize() {
    var nWidth = $(this).width() + $(this).scrollLeft(),
      nHeight = $(this).height() + $(this).scrollTop();

    return { width: nWidth, height: nHeight };
  }

  // login to nvr
  public loginToNvr(nvr: Nvr): Promise<number> {
    return new Promise((resolve) => {

      var szIP = nvr.ip,
        szPort = nvr.port,
        szUsername = nvr.userName,
        szPassword = nvr.password;

      if (szIP == '' || szIP == null || !szPort)
        return;

      var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
        success: (xmlDoc) => {
          resolve(1);
        },
        error: (err, xmlDoc) => {
          resolve(0);
        }
      });

      if (-1 == iRet) {
        resolve(2);
      }
    });

  }

  // logout from nvr
  public logout(nvr: Nvr): Promise<VideoPreviewMessage> {
    return new Promise(async (resolve) => {
      var szIP = nvr.ip;

      if (szIP == '' || szIP == null)
        return;

      var iRet = WebVideoCtrl.I_Logout(szIP);
      console.log(iRet);
      if (0 == iRet) {
        console.log('logout success');
        resolve({
          status: VideoPerviewStatus.NVR_LOGOUT_SUCCESS,
          success: true
        });
      }
      else {
        console.log('logout failed');
        resolve({
          status: VideoPerviewStatus.NVR_LOGOUT_FAILED,
          success: false
        });
      }
    });
  }

  public async playVideo(camera: Camera, nvr: Nvr): Promise<VideoPreviewMessage> {
    return new Promise(async (resolve, reject) => {
      let loginState = await this.loginToNvr(nvr);

      switch (loginState) {
        // Login failed
        case 0:
          {
            console.log('Login to nvr failed!');
            this.toasterService.showError('Login to NVR failed.');
            resolve({
              status: VideoPerviewStatus.NVR_LOGIN_FAILED,
              success: false
            });

            break;
          }
        // Login success -> show the selected camera
        case 1:
          {
            // Important timeout!!! The WebVideoCtrl need to render the DOM !!!
            setTimeout(async () => {
              console.log('Success to Logged in to nvr!');
              let realPlayState = await this.startRealPlay(camera, nvr);

              if (realPlayState == 0) {
                console.log('Playing video');
                resolve({
                  status: VideoPerviewStatus.SUCCESS_PLAYING_VIDEO,
                  success: true
                });
              }
              else {
                console.log('Cannot play video');
                resolve({
                  status: VideoPerviewStatus.FAILED_PLAYING_VIDEO,
                  success: false
                });
              }

            }, 1000);

            break;
          }
        // Already logged in
        case 2:
          {

            console.log('Already logged in to nvr');
            let realPlayState = await this.startRealPlay(camera, nvr);
            if (realPlayState == 0) {
              console.log('Playing video');
              resolve({
                status: VideoPerviewStatus.SUCCESS_PLAYING_VIDEO,
                success: true
              });
            }
            else {
              console.log('Cannot play video');
              resolve({
                status: VideoPerviewStatus.FAILED_PLAYING_VIDEO,
                success: false
              });
            }

            break;
          }
      }
    });
  }

  // start real play
  public startRealPlay(camera: Camera, nvr: Nvr): Promise<number> {
    return new Promise((resolve) => {

      var oWndInfo = WebVideoCtrl.I_GetWindowStatus(this.g_iWndIndex),
        szDeviceIdentify = nvr.ip,
        iStreamType = 1,
        iRtspPort = nvr.rtspPort,
        iChannelID = camera.channel,
        bZeroChannel = false;

      if (szDeviceIdentify == '' || szDeviceIdentify == null)
        return;

      var startRealPlay = () => {
        WebVideoCtrl.I_StartRealPlay(szDeviceIdentify, {
          iRtspPort: iRtspPort,
          iStreamType: iStreamType,
          iChannelID: iChannelID,
          bZeroChannel: bZeroChannel,
          success: () => {
            resolve(0);
          },
          error: (status, xmlDoc) => {
            resolve(1);
          }
        });
      };

      // stop play first
      if (oWndInfo != null) {
        WebVideoCtrl.I_Stop({
          success: () => {
            startRealPlay();
          }
        });
      } else {
        startRealPlay();
      }
    });
  }

  // stop video
  public stopVideo(): Promise<VideoPreviewMessage> {
    return new Promise(async (resolve, reject) => {
      var oWndInfo = WebVideoCtrl.I_GetWindowStatus(this.g_iWndIndex);

      if (oWndInfo != null) {
        WebVideoCtrl.I_Stop({
          success: () => {
            console.log('stop real play success');
            resolve({
              status: VideoPerviewStatus.SUCCESS_STOPING_VIDEO,
              success: true
            });
          },
          error: () => {
            console.log('stop real play failed');
            resolve({
              status: VideoPerviewStatus.FAILED_STOPING_VIDEO,
              success: false
            });
          }
        });
      }
    });
  }

  // open sound
  public openSound(): Promise<VideoPreviewMessage> {
    return new Promise(async (resolve) => {
      var oWndInfo = WebVideoCtrl.I_GetWindowStatus(this.g_iWndIndex);

      if (oWndInfo != null) {
        var allWndInfo = WebVideoCtrl.I_GetWindowStatus();
        // close the sound by iterating over all the window
        for (var i = 0, iLen = allWndInfo.length; i < iLen; i++) {
          oWndInfo = allWndInfo[i];
          if (oWndInfo.bSound) {
            WebVideoCtrl.I_CloseSound(oWndInfo.iIndex);
            break;
          }
        }

        var iRet = WebVideoCtrl.I_OpenSound();

        if (0 == iRet) {
          console.log('open sound success！');
          resolve({
            status: VideoPerviewStatus.SUCCESS_OPEN_SOUND,
            success: true
          });
        }
        else {
          console.log('open sound failed！');
          resolve({
            status: VideoPerviewStatus.FAILED_OPEN_SOUND,
            success: false
          });
        }
      }
    });
  }

  // close sound
  public closeSound(): Promise<VideoPreviewMessage> {
    return new Promise(async (resolve) => {
      var oWndInfo = WebVideoCtrl.I_GetWindowStatus(this.g_iWndIndex);

      if (oWndInfo != null) {
        var iRet = WebVideoCtrl.I_CloseSound();
        if (0 == iRet) {
          console.log('close sound success！');
          resolve({
            status: VideoPerviewStatus.SUCCESS_CLOSE_SOUND,
            success: true
          });
        }
        else {
          console.log('close sound failed！');
          resolve({
            status: VideoPerviewStatus.FAILED_CLOSE_SOUND,
            success: false
          });
        }
      }
    });
  }

  // windows number
  public changeWndNum(iType) {
    iType = parseInt(iType, 10);
    WebVideoCtrl.I_ChangeWndNum(iType);
  }


  // get IP channel
  public getDigitalChannelInfo(nvr: Nvr) {
    var szDeviceIdentify = nvr.ip + '_' + nvr.port;
    var iAnalogChannelNum = 0;

    if (null == szDeviceIdentify) {
      return;
    }

    // analog channel
    WebVideoCtrl.I_GetAnalogChannelInfo(szDeviceIdentify, {
      async: false,
      success: function (xmlDoc) {
        iAnalogChannelNum = $(xmlDoc).find('VideoInputChannel').length;

      },
      error: function () {

      }
    });

    // IP channel
    WebVideoCtrl.I_GetDigitalChannelInfo(szDeviceIdentify, {
      async: false,
      success: (xmlDoc) => {
        var oChannels = $(xmlDoc).find('InputProxyChannelStatus');

        $.each(oChannels, function () {
          var id = parseInt($(this).find('id').eq(0).text(), 10),
            ipAddress = $(this).find('ipAddress').eq(0).text(),
            srcInputPort = $(this).find('srcInputPort').eq(0).text(),
            managePortNo = $(this).find('managePortNo').eq(0).text(),
            online = $(this).find('online').eq(0).text(),
            proxyProtocol = $(this).find('proxyProtocol').eq(0).text();

          console.log('IP:' + ipAddress);
          console.log('online:' + online);
        });


      },
      error: (status, xmlDoc) => {

      }
    });
  }


  public clickSetVolume() {

  }

  public clickCapturePic() {

  }

  public clickStartRecord() {

  }

  public clickStopRecord() {

  }

  public clickStartVoiceTalk() {

  }

  public clickStopVoiceTalk() {

  }

  public clickEnableEZoom() {

  }

  public clickDisableEZoom() {

  }

  // // time format
  // private dateFormat(oDate, fmt) {
  //   var o = {
  //     "M+": oDate.getMonth() + 1, //month
  //     "d+": oDate.getDate(), //day
  //     "h+": oDate.getHours(), //hour
  //     "m+": oDate.getMinutes(), //minute
  //     "s+": oDate.getSeconds(), //second
  //     "q+": Math.floor((oDate.getMonth() + 3) / 3), //quarter
  //     "S": oDate.getMilliseconds()//millisecond
  //   };
  //   if (/(y+)/.test(fmt)) {
  //     fmt = fmt.replace(RegExp.$1, (oDate.getFullYear() + "").substr(4 - RegExp.$1.length));
  //   }
  //   for (var k in o) {
  //     if (new RegExp("(" + k + ")").test(fmt)) {
  //       fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  //     }
  //   }
  //   return fmt;
  // }

  // private addLog(value: string) {
  //   //$("#logs").prepend('<li class="list-group-item">' + this.dateFormat(new Date(), "yyyy-MM-dd hh:mm:ss") + ': ' + value + '</li>');
  // }

}
