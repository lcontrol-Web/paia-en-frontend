import { ConfirmationService } from 'primeng/api';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
  providers: [ConfirmationService],

})
export class ButtonComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('button') button: ElementRef;

  @Input() disabled: boolean = false;
  @Input() description: string;
  @Input() class: string;
  @Input() type: string = 'button';
  @Input() icon: string;
  @Input() tooltipPosition: string = 'top';
  @Input() tooltipTitle: string = '';
  @Input() confirm: boolean = false;
  @Input() confirmMessage: string;
  @Input() confirmIcon: string = 'fas fa-exclamation-triangle';
  @Output('onClick') private clickEmitter: EventEmitter<any> = new EventEmitter();
  @Output('onConfirm') private confirmEmitter: EventEmitter<any> = new EventEmitter();
  @Output('onRejectConfirm') private rejectConfirmEmitter: EventEmitter<any> = new EventEmitter();

  constructor(private confirmationService: ConfirmationService,) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $(this.button.nativeElement).tooltip();
  }

  ngOnDestroy() {
    $(this.button.nativeElement).tooltip('hide');
  }

  public onClick(event) {
    $(this.button.nativeElement).tooltip('hide');
    if (this.confirm)
      this.openConfirm(event);

    this.clickEmitter.emit();
  }

  public openConfirm(event: Event) {
    this.confirmationService.confirm({
      target: event.target,
      message: this.confirmMessage,
      icon: this.confirmIcon,
      accept: () => {
        this.confirmEmitter.emit();
      },
      reject: () => {
        this.rejectConfirmEmitter.emit();
      }
    });
  }
}
