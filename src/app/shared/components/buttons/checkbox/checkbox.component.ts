import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() public label: string = '';
  @Input() public checked: boolean = false;
  @Input() public required: boolean = false;
  @Input() public disabled: boolean = false;
  @Input() public name: string;
  @Input() public styleClass: string;
  @Output('onChecked') checkedEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { }

  public onChecked() {
    this.checkedEmitter.emit(this.checked);
  }
}
