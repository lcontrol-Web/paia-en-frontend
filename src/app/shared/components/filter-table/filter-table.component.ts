import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { FilterActionTypes } from 'src/app/core/enums/filter-action-types.enum';
import { FilterCondition } from 'src/app/core/enums/filter-condition.enum';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { FilterTableMessage } from 'src/app/core/models/filter-table-message';
import { SortAction } from 'src/app/core/models/sort-action';
import { FilterTableService } from 'src/app/core/services/filter-table.service';
declare var $: any;

@Component({
  selector: 'app-filter-table',
  templateUrl: './filter-table.component.html',
  styleUrls: ['./filter-table.component.css']
})
export class FilterTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() public name: string;
  @Input() public type: DataTypes;
  @Input() public property: string;
  @Input() public filterNumber: number;
  @Input() public class: string;
  @Output('onSort') sortEmitter: EventEmitter<SortAction> = new EventEmitter<SortAction>();
  @Output('onFilter') filterEmitter: EventEmitter<FilterAction> = new EventEmitter<FilterAction>();
  @Output('onClearSort') clearSortEmitter: EventEmitter<any> = new EventEmitter();
  @Output('onClearFilter') clearFilterEmitter: EventEmitter<any> = new EventEmitter();

  public filterEnabled: boolean = false;
  public ascendingEnabled: boolean = false;
  public descendingEnabled: boolean = false;
  public searchText: string;
  public condition: FilterCondition = FilterCondition.Contain;
  private subscription: Subscription;

  constructor(private filterTableService: FilterTableService) {
    this.subscribeForMessages();
  }

  ngOnInit() {

  }

  ngAfterViewInit() {

    // for not closing the dropdown menu when we pressed on it
    $('#firstOption' + this.filterNumber).on('click', function (event) {
      event.stopPropagation();
    });

    $('#operator' + this.filterNumber).on('click', function (event) {
      event.stopPropagation();
    });

    $('#secondOption' + this.filterNumber).on('click', function (event) {
      event.stopPropagation();
    });
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  private subscribeForMessages() {

    this.subscription = this.filterTableService.getMessage()
      .subscribe((message: FilterTableMessage) => {

        if (message.action === FilterActionTypes.ClearFilters) {
          this.filterEnabled = false;
          this.ascendingEnabled = false;
          this.descendingEnabled = false;
        }
        else if (message.filterNumber != this.filterNumber) {
          switch (message.action) {
            case FilterActionTypes.SortByColumn:
            case FilterActionTypes.Clear:
              this.ascendingEnabled = false;
              this.descendingEnabled = false;
              break;
            case FilterActionTypes.FilterByColumn:
              this.filterEnabled = false;
              break;

            default:
              break;
          }
        }
      });
  }

  public filter() {
    this.filterEnabled = true;
    this.filterTableService.onFilter(this.filterNumber);
    this.filterEmitter.emit({
      searchText: this.searchText,
      condition: this.condition,
      column: this.property,
    });
  }

  public asc() {
    this.ascendingEnabled = true;
    this.descendingEnabled = false;
    this.filterTableService.onSort(this.filterNumber);
    this.sortEmitter.emit({
      sort: SortTypes.Ascending,
      column: this.property
    });
  }

  public desc() {
    this.descendingEnabled = true;
    this.ascendingEnabled = false;
    this.filterTableService.onSort(this.filterNumber);
    this.sortEmitter.emit({
      sort: SortTypes.Descending,
      column: this.property
    });
  }

  public clearSort() {
    this.ascendingEnabled = false;
    this.descendingEnabled = false;
    this.clearSortEmitter.emit();
  }

  public clearFilter() {
    this.filterEnabled = false;
    this.filterTableService.onClear(this.filterNumber);
    this.clearFilterEmitter.emit();
  }
}

