import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input-group-select',
  templateUrl: './input-group-select.component.html',
  styleUrls: ['./input-group-select.component.css']
})
export class InputGroupSelectComponent implements OnInit {

  @Input() public selected: any;
  @Input() public label: string;
  @Input() public required: boolean = false;
  @Input() public styleClass: string;
  @Input() public inputStyleClass: string;
  @Input() public options: any[] = [];
  @Input() public description: string = '';
  @Output('onChange') changeEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  public onChange() {
    this.changeEmitter.emit(this.selected);
  }
}
