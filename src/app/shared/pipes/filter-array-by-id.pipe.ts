import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterArrayById'
})
export class FilterArrayByIdPipe implements PipeTransform {

  transform(items: any[], itemId: string | number, itemKey: string, trigger?: boolean): any[] {

    if (!items)
      return [];

    if (!itemId || !itemKey)
      return items;

    return items.filter(item => item[itemKey] == itemId);
  }

}
