import { PerviewCameraComponent } from './components/perview-camera/perview-camera.component';
import { InputGroupSelectComponent } from './components/input-group-select/input-group-select.component';
import { LoaderComponent } from './components/loader/loader.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { CheckboxComponent } from './components/buttons/checkbox/checkbox.component';
import { FilterTableComponent } from './components/filter-table/filter-table.component';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/buttons/button/button.component';
import { LoadingButtonComponent } from './components/buttons/loading-button/loading-button.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ItemsPageAmountComponent } from './components/items-page-amount/items-page-amount.component';
import { SearchTableComponent } from './components/search-table/search-table.component';
import { DeleteFormModalComponent } from './components/delete-form-modal/delete-form-modal.component';
import { FilterArrayByIdPipe } from './pipes/filter-array-by-id.pipe';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { NiceDateFormatPipe } from './pipes/nice-date-format.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    FilterTableComponent,
    ButtonComponent,
    LoadingButtonComponent,
    CheckboxComponent,
    LoaderComponent,
    ItemsPageAmountComponent,
    SearchTableComponent,
    DeleteFormModalComponent,
    InputGroupSelectComponent,
    PerviewCameraComponent,

    CapitalizePipe,
    FilterArrayByIdPipe,
    SanitizeHtmlPipe,
    NiceDateFormatPipe
  ],
  exports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    
    FilterTableComponent,
    ButtonComponent,
    LoadingButtonComponent,
    CheckboxComponent,
    LoaderComponent,
    ItemsPageAmountComponent,
    SearchTableComponent,
    DeleteFormModalComponent,
    InputGroupSelectComponent,
    PerviewCameraComponent,

    CapitalizePipe,
    FilterArrayByIdPipe,
    SanitizeHtmlPipe,
    NiceDateFormatPipe
  ],
  providers: []
})
export class SharedModule { }
