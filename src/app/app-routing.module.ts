import { LoggedGuard } from './core/guards/logged.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './core/components/login/login.component';
import { MenuComponent } from './core/components/menu/menu.component';
import { NotAuthorizedComponent } from './core/components/not-authorized/not-authorized.component';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { AuthGuard } from './core/guards/auth.guard';
import { AdministratorGuard } from './core/guards/administrator.guard';
import { TeamLeaderAndHigherGuard } from './core/guards/teamLeaderAndHigher.guard';
import { NoPermissionGuard } from './core/guards/noPermission.guard';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [LoggedGuard] },
  { path: 'menu', component: MenuComponent, canActivate: [AuthGuard] },
  {
    path: 'user-groups',
    canActivate: [AuthGuard, AdministratorGuard],
    loadChildren: () => import('./modules/user-groups/user-groups.module').then(m => m.UserGroupsModule)
  },
  {
    path: 'users',
    canActivate: [AuthGuard, TeamLeaderAndHigherGuard],
    loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'nvrs',
    canActivate: [AuthGuard, AdministratorGuard],
    loadChildren: () => import('./modules/nvrs/nvrs.module').then(m => m.NvrsModule)
  },
  {
    path: 'cameras',
    canActivate: [AuthGuard, AdministratorGuard],
    loadChildren: () => import('./modules/cameras/cameras.module').then(m => m.CamerasModule)
  },
  {
    path: 'user-interface',
    canActivate: [AuthGuard, TeamLeaderAndHigherGuard],
    loadChildren: () => import('./modules/user-interface/user-interface.module').then(m => m.UserInterfaceModule)
  },
  {
    path: 'interrogations',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/interrogations/interrogations.module').then(m => m.InterrogationsModule)
  },
  {
    path: 'active-interrogations',
    canActivate: [AuthGuard, TeamLeaderAndHigherGuard],
    loadChildren: () => import('./modules/active-interrogations/active-interrogations.module').then(m => m.ActiveInterrogationsModule)
  },
  {
    path: 'interrogation-records',
    canActivate: [AuthGuard, NoPermissionGuard],
    loadChildren: () => import('./modules/interrogation-records/interrogation-records.module').then(m => m.InterrogationRecordsModule)
  },
  {
    path: 'logs',
    canActivate: [AuthGuard, TeamLeaderAndHigherGuard],
    loadChildren: () => import('./modules/logs/logs.module').then(m => m.LogsModule)
  },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: 'not-authorized', component: NotAuthorizedComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
