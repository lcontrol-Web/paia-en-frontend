import { FilterCondition } from './../enums/filter-condition.enum';

export class FilterAction {
    public searchText: string;
    public condition: FilterCondition;
    public column: string;
}
