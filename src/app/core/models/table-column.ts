import { DataTypes } from '../enums/data-types.enum';

export class TableColumn {
    public name: string;
    public type: DataTypes;
    public property: string;
    public fit?: boolean = false;
}
