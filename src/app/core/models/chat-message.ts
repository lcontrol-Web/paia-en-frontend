export class ChatMessage {
    public class: string;
    public imgSrc: string;
    public userName: string;
    public userId: number;
    public text: string;
    public date: number;
    public error: boolean = false;
}
