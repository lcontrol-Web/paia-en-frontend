import { UserGroupTypes } from './../enums/user-group-types.enum';

export class LoggedUser {
    public userId: number;
    public userName: string;
    public userGroupId: number;
    public userGroupName: string;
    public userGroupType: UserGroupTypes;
}

