import { FilterCondition } from './../enums/filter-condition.enum';
import { SortTypes } from '../enums/sort-types.enum';

export class Pagination {
    public page: number = 1;
    public itemsPerPage: number = 10;
    public searchText: string;
    public sort: string = SortTypes.Descending;
    public column: string;
    public condition: FilterCondition;
}
