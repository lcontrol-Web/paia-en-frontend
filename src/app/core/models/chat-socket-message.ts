export class ChatSocketMessage {
    public to: string;
    public from: string;
    public text: string;
    public userId: number;
    public userName: string;
}


