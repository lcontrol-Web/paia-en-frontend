import { SortTypes } from './../enums/sort-types.enum';
export class SortAction {
    public sort: SortTypes;
    public column: string;
}
