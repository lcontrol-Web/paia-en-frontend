import { FilterActionTypes } from '../enums/filter-action-types.enum';

export class FilterTableMessage {
    action: FilterActionTypes;
    filterNumber?: number;
}
