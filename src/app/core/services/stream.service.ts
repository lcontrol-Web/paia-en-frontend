import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class StreamService {

  private apiName = 'stream';

  constructor(private dataService: DataService) { }

  public streamFile(): Observable<any> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getStream();
  }

  public getFilesInsideFolder(interrogationId: number): Observable<string[]> {
    this.dataService.apiName = `${this.apiName}/${interrogationId}/files-inside-folder`;
    return this.dataService.getData();
  }
}
