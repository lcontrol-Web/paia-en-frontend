import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Pagination } from '../models/pagination';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {

  constructor() { }

  public getPaginationParams(pagination: Pagination) : HttpParams {
    let params = new HttpParams();
    if (pagination) {
      if (pagination.itemsPerPage)
        params = params.append('limit', pagination.itemsPerPage.toString());
      if (pagination.page)
        params = params.append('page', pagination.page.toString());
      if (pagination.searchText)
        params = params.append('searchText', pagination.searchText.toString());
      if (pagination.sort)
        params = params.append('sort', pagination.sort.toString());
      if (pagination.column)
        params = params.append('column', pagination.column.toString());
      if (pagination.condition)
        params = params.append('condition', pagination.condition.toString());
    }

    return params;
  }
}
