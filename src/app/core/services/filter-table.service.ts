import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { FilterActionTypes } from '../enums/filter-action-types.enum';
import { FilterTableMessage } from '../models/filter-table-message';

@Injectable({
  providedIn: 'root'
})
export class FilterTableService {

  private subject = new Subject<any>();
 
  constructor() { }

  private sendMessage(message: FilterTableMessage) {
    this.subject.next(message);
  }

  public getMessage(): Observable<FilterTableMessage> {
    return this.subject.asObservable();
  }

  public onFilter(filterNumber: number) {
    this.sendMessage({
      action: FilterActionTypes.FilterByColumn,
      filterNumber: filterNumber
    });
  }

  public onSort(filterNumber: number) {
    this.sendMessage({
      action: FilterActionTypes.SortByColumn,
      filterNumber: filterNumber
    });
  }

  public onClear(filterNumber: number) {
    this.sendMessage({
      action: FilterActionTypes.Clear,
      filterNumber: filterNumber
    });
  }

  public onSearchTableFilter() {
    this.sendMessage({
      action: FilterActionTypes.ClearFilters
    });
  }
}
