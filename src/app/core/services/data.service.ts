import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public apiName: string;

  constructor(private http: HttpClient) {
  }

  public getData(params?: HttpParams): Observable<any> {

    if (params) {
      params = params.append('timestamp', (+new Date()).toString());
    }
    else {
      params = new HttpParams()
        .set('timestamp', (+new Date()).toString());
    }

    return this.http.get(environment.apiUrl + '/api/' + this.apiName, { params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataById(id: number, params?: HttpParams): Observable<any> {

    if (params) {
      params = params.append('timestamp', (+new Date()).toString());
    }
    else {
      params = new HttpParams()
        .set('timestamp', (+new Date()).toString());
    }

    return this.http.get(environment.apiUrl + '/api/' + this.apiName + '/' + id, { params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getStream(params?: HttpParams): Observable<any> {

    if (params) {
      params = params.append('timestamp', (+new Date()).toString());
    }
    else {
      params = new HttpParams()
        .set('timestamp', (+new Date()).toString());
    }

    return this.http.get(environment.apiUrl + '/api/' + this.apiName, { params: params, responseType: 'blob' }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataByDateRange(data: Date[], params?: HttpParams): Observable<any> {
    return this.http.post(environment.apiUrl + '/api/' + this.apiName, data, { params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataByObject(data: any, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(environment.apiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers, params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public add(data: any): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(environment.apiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers }).pipe(
      map((response: Response) => {
        return response;
      }));
  }

  public update(paramValue: string | number, data: any): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put(environment.apiUrl + '/api/' + this.apiName + '/' + paramValue, JSON.stringify(data), { headers: headers }).pipe(
      map(
        (response: Response) => {
          return response;
        }));
  }

  public delete(paramValue: string | number): Observable<number> {

    return this.http.delete<string>(environment.apiUrl + '/api/' + this.apiName + '/' + paramValue).pipe(
      map(
        (response: string) => {
          return +response;
        }));
  }

  public multipleDelete(data: string[] | number[]): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(environment.apiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers }).pipe(
      map(
        (response: Response) => {
          return response;
        }));
  }

  public getApiName(): string {
    return this.apiName;
  }

}
