import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  public addItemToItems(items: any[], item: any) {

    if (!items || !item)
      return;

    items.unshift(item);
  }

  public addItemToItemsByCopy([...items]: any[], item: any): any[] {
    if (!items || !item)
      return items;
    items.unshift(item);
    return items;
  }

  public updateItemInItems(items: any[], item: any, key: string) {

    if (!items || !item)
      return;

    const index = items.findIndex((element) => element[key] === item[key]);
    if (index > -1)
      items[index] = item;
  }

  public updateItemInItemsByCopy([...items]: any[], item: any, key: string): any[] {

    if (!items || !item)
      return items;

    const index = items.findIndex((element) => element[key] === item[key]);
    if (index > -1)
      items[index] = item;

    return items;
  }

  public deleteItemFromItems(items: any[], item: any, key: string) {

    if (!items || !item)
      return;

    const index = items.findIndex(x => x[key] === item[key]);
    if (index > -1)
      items.splice(index, 1);
  }

  public deleteItemFromItemsByCopy([...items]: any[], item: any, key: string) {

    if (!items || !item)
      return items;

    const index = items.findIndex(x => x[key] === item[key]);
    if (index > -1)
      items.splice(index, 1);

    return items;
  }

  public deleteItemsFromItemsByCopy([...items]: any[], deletedItems: any[], key: string): any[] {

    if (!items || !deletedItems)
      return items;

    const newItems = items.filter((e) => {
      return !deletedItems.some((s) => {
        return s[key] === e[key];
      });
    });

    return newItems;
  }

  public getParentValueById(id: string, items: any[], key: string) {

    if (!id || !items)
      return;

    let foutndedItem = items.find((item) => {
      if (id === item[key])
        return item;
    });

    return foutndedItem;
  }

  public arraysAreEqual(array1: any[], array2: any[], key: string): boolean {
    // return the ones with equal key
    var result = array1.filter((o1) => {
      return array2.some((o2) => {
        return o1[key] === o2[key];
      });
    });

    if (result.length === array2.length)
      return true;
    return false;

  }
}
