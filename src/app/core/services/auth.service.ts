import { environment } from '../../../environments/environment.prod';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import { Credentials } from '../models/credentials';
import { LoggedUser } from '../models/logged-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // this variable responsibale for sending messages (loggedin / loggedout) for the subscribers
  // also holds the value
  private currentUserSubject: BehaviorSubject<LoggedUser>;

  // this variable returns an obesrvable
  public currentLoggedUser: Observable<LoggedUser>;

  constructor(private http: HttpClient,
    private router: Router) {
    let token = this.getToken();
    let loggedUser;

    if (token)
      loggedUser = this.getDecodedAccessToken(token);

    this.currentUserSubject = new BehaviorSubject<LoggedUser>(loggedUser);
    this.currentLoggedUser = this.currentUserSubject.asObservable();
  }

  public login(credentials: Credentials): Observable<boolean> {

    return this.http.post(`${environment.apiUrl}/api/auth`, credentials, { observe: 'response' })
      .pipe(map(
        (response: HttpResponse<LoggedUser>) => {
          let token = response.headers.get('x-auth-token');
          if (token) {
            localStorage.setItem('token', token);
            this.currentUserSubject.next(response.body);
            return true;
          }
          else
            return false;
        }));
  }

  public logout() {
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
    this.router.navigate(['']);
  }

  public getCurrentUserValue(): LoggedUser {
    return this.currentUserSubject.value;
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  private getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }
}



