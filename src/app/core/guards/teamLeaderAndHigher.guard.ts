import { UserGroupTypes } from '../enums/user-group-types.enum';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class TeamLeaderAndHigherGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const loggedUser = this.authService.getCurrentUserValue();

        if (loggedUser.userGroupType !== UserGroupTypes.Administrator && loggedUser.userGroupType !== UserGroupTypes.TeamLeader) {
            this.router.navigate(['/not-authorized']);
            return false;
        }
        else
            return true;
    }
}
