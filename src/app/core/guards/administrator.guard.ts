import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserGroupTypes } from '../enums/user-group-types.enum';

@Injectable({
  providedIn: 'root'
})
export class AdministratorGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const loggedUser = this.authService.getCurrentUserValue();

    if (loggedUser.userGroupType !== UserGroupTypes.Administrator) {
      this.router.navigate(['/not-authorized']);
      return false;
    }
    else
      return true;
  }
}
