import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { InterrogationsComponent } from 'src/app/modules/interrogations/interrogations.component';

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateInterrogationGuard implements CanDeactivate<InterrogationsComponent> {

  component: Object;
  route: ActivatedRouteSnapshot;

  constructor() { }

  canDeactivate(interrogationsComponent: InterrogationsComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
    return interrogationsComponent.canExitComponent();

  }
}
