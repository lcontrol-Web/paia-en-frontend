import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserGroupTypes } from '../enums/user-group-types.enum';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class NoPermissionGuard {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const loggedUser = this.authService.getCurrentUserValue();

    if (loggedUser.userGroupType === UserGroupTypes.NoPermission) {
      this.router.navigate(['/not-authorized']);
      return false;
    }
    else
      return true;
  }

}
