import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserGroupTypes } from '../enums/user-group-types.enum';

@Injectable({
    providedIn: 'root'
})
export class ManagerAndHigherGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const loggedUser = this.authService.getCurrentUserValue();

        if (loggedUser.userGroupType !== UserGroupTypes.Administrator &&
            loggedUser.userGroupType !== UserGroupTypes.TeamLeader &&
            loggedUser.userGroupType !== UserGroupTypes.Manager) {
            this.router.navigate(['/not-authorized']);
            return false;
        }
        else
            return true;
    }
}
