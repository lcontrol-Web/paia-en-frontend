import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoggedUser } from '../../models/logged-user';
declare var WebVideoCtrl: any;

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {

  public loggedUser: LoggedUser;
  public pluginInstalled: boolean = true;

  constructor(public authService: AuthService) {
    this.authService.currentLoggedUser
      .subscribe(
        (loggedUser: LoggedUser) => {
          this.loggedUser = loggedUser;
        });
  }

  ngOnInit(): void {
    if (-1 == WebVideoCtrl.I_CheckPluginInstall())
      this.pluginInstalled = false;
  }

  logout() {
    this.authService.logout();
  }

}
