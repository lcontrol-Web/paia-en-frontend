import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoggedUser } from '../../models/logged-user';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public loggedUser: LoggedUser;

  constructor(public authService: AuthService) {

    this.authService.currentLoggedUser
      .subscribe(
        (loggedUser: LoggedUser) => {
          this.loggedUser = loggedUser;
        });
  }

  ngOnInit() {
  }
}
