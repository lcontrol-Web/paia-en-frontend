import { ToasterService } from './../../services/toaster.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Credentials } from '../../models/credentials';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public credentials: Credentials = new Credentials();
  public rememberMe: boolean = false;
  public isLoading: boolean = false;
  public showPass: boolean = false;

  constructor(private authService: AuthService,
    private toasterService: ToasterService,
    private router: Router) { }

  ngOnInit() { }

  public async login() {
    this.isLoading = true;
    this.authService.login(this.credentials)
      .subscribe(
        (loogedIn: boolean) => {

          if (loogedIn)
            this.router.navigate(['/menu']);
        },
        (error: HttpErrorResponse) => {
          this.toasterService.showError('Incorrect user name or password.');

          this.isLoading = false;
        });
  }

}
