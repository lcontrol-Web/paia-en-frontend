export enum FilterActionTypes {
    SortByColumn = 'sort-by-column',
    FilterByColumn = 'filter-by-column',
    Clear = 'clear',
    ClearFilters = 'clear-filters',
    ClearAll = 'clear-all',
}
