export enum SortTypes {
    Descending = 'desc',
    Ascending = 'asc'
}
