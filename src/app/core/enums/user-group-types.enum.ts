export enum UserGroupTypes {
    Administrator = 'administrator',
    TeamLeader = 'team leader',
    Investigator = 'investigator',
    Manager = 'manager',
    NoPermission = 'no permission',
}
