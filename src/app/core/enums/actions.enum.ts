export enum Actions {
    Add = 'add',
    Update = 'update',
    Delete = 'delete',
}
