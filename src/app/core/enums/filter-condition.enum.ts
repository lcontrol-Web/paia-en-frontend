export enum FilterCondition {
    Contain = 1,
    NotContain = 2,
    StartsWith = 3,
    EndsWith = 4,
    Equal = 5,
}
