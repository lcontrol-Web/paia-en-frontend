export enum DataTypes {
    String = 'string',
    Number = 'number',
    Boolean = 'boolean',
    Array = 'array',
}
