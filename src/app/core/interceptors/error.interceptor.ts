import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(error => {
                
                if (error instanceof HttpErrorResponse) {

                    let err : string;

                    if (!error.status) {
                        err = 'internalServerError';
                    }
                    else if (error.status === 404) {
                        err = 'resourceNotFound';
                    }
                    else if (error.status === 401) {
                        err = error.error || error.statusText;
                    } else if (err === 'ExpiredToken') {
                        // TO DO - logout
                        err = error.error || error.statusText;
                    } else {
                        err = error.error || error.statusText;
                    }

                    return throwError(err);
                }
                else
                    return null
            })
        );

    }

}
