import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserInterfaceComponent } from './user-interface.component';

const routes: Routes = [
  {
    path: '',
    component: UserInterfaceComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [UserInterfaceComponent]
})
export class UserInterfaceModule { }
