import { ChatSocketMessage } from './../../core/models/chat-socket-message';
import { environment } from './../../../environments/environment';
import { ToasterService } from './../../core/services/toaster.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { LoggedUser } from 'src/app/core/models/logged-user';
import { AuthService } from 'src/app/core/services/auth.service';
import { Camera } from '../cameras/models/camera';
import { CameraService } from '../cameras/services/camera.service';
import { Nvr } from '../nvrs/models/nvr';
import { NvrService } from '../nvrs/services/nvr.service';
import { User } from '../users/models/user';
import { UserService } from '../users/services/user.service';
import { io, Socket } from 'socket.io-client';
import { UserGroupTypes } from 'src/app/core/enums/user-group-types.enum';
import { VideoPerviewStatus } from 'src/app/core/enums/video-perview-status.enum';
import { VideoPreviewMessage } from 'src/app/core/models/video-preview-message';
import { PerviewCameraComponent } from 'src/app/shared/components/perview-camera/perview-camera.component';
import { ActiveInterrogation } from '../interrogations/models/active-interrogation';
declare var $: any;

@Component({
  selector: 'app-user-interface',
  templateUrl: './user-interface.component.html',
  styleUrls: ['./user-interface.component.css']
})
export class UserInterfaceComponent implements OnInit, AfterViewInit {

  @ViewChild(PerviewCameraComponent, { static: false }) perviewCameraComponent: PerviewCameraComponent;

  public dataIsReady: boolean = true;
  public nvrs: Nvr[] = [];
  public cameras: Camera[] = [];
  public selectedCamera: Camera;
  public selectedNvr: Nvr;
  public amountOfScreens: string[] = ['1x1', '2x2', '3x3', '4x4'];
  public selectedAmountOfScreens: number = 2;
  public liveInterrogations: ActiveInterrogation[] = [];
  public socketDisconnected: boolean = false;
  private loggedUser: LoggedUser;
  private users: User[] = [];
  private socket: Socket;

  constructor(private nvrService: NvrService,
    private cameraService: CameraService,
    private userService: UserService,
    private toasterService: ToasterService,
    private authService: AuthService) {
    this.loggedUser = this.authService.getCurrentUserValue();
  }

  ngOnInit() {
    this.getData();
  }

  ngAfterViewInit(): void {

    $(function () {
      $('#menu-toggle').click(function (e) {
        e.preventDefault();
        $('#wrapper').toggleClass('toggled');
      });

      $(window).resize(function (e) {
        if ($(window).width() <= 768) {
          $('#wrapper').removeClass('toggled');
        } else {
          $('#wrapper').addClass('toggled');
        }
      });
    });
  }

  private getData() {

    let httpReq = [];
    httpReq.push(this.nvrService.getNvrs());
    httpReq.push(this.cameraService.getCameras());

    if (this.loggedUser.userGroupType === UserGroupTypes.TeamLeader)
      httpReq.push(this.userService.getUsers());

    forkJoin(httpReq)
      .subscribe(
        (response: any[]) => {
          this.nvrs = response[0];
          this.cameras = response[1];

          if (this.loggedUser.userGroupType === UserGroupTypes.TeamLeader)
            this.users = response[2];

          this.setupSocketConnection();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  private setupSocketConnection() {
    this.socket = io(environment.socketUrl, {
      query: {
        clientType: 1
      }
    });

    this.socket.on('connect', () => {
      console.log('Connected to socket');
    });

    this.socket.on('message', (message: ChatSocketMessage) => {
      // check if this is not our socket send the message
      // if (message.from != this.socket.id)
      //   this.liveInterogattionModalComponent.addMessage(true, message.text, message.userName, message.userId);
    });

    this.socket.on('connected-to-room', (roomName: string) => {
      console.log('You are connected to room: ' + roomName);
    });

    this.socket.on('live-interrogations', (liveInterrogations: ActiveInterrogation[]) => {
      this.setLiveInterrigations(liveInterrogations);
    });

    this.socket.on('connect_error', (err) => {

    });

    this.socket.on('error', (err) => {
      console.log(err);
    });

    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });
  }

  public onCameraSelected(camera: Camera, nvr: Nvr) {
    this.selectedCamera = camera;
    this.selectedNvr = nvr;
  }

  private setLiveInterrigations(liveInterrogations: ActiveInterrogation[]) {

    // remove the old sockets
    for (let i = 0; i < this.liveInterrogations.length; i++) {
      if (!liveInterrogations.some(li => li.socketId === this.liveInterrogations[i].socketId)) {
        this.changeIsActiveToNvrAndCamera(this.liveInterrogations[i], false);
        this.liveInterrogations.splice(i, 1);
      }
    }

    // add the new sockets
    for (const liveInterrogation of liveInterrogations) {
      if (!this.liveInterrogations.some(li => li.socketId === liveInterrogation.socketId)) {

        // show only the live interrogations that the team leader can see (by his users) or level 5
        if (this.loggedUser.userGroupType === UserGroupTypes.TeamLeader &&
          this.users.some((user: User) => user.userId === liveInterrogation.userId) || this.loggedUser.userGroupType === UserGroupTypes.Administrator) {
          this.liveInterrogations.push(liveInterrogation);
          this.changeIsActiveToNvrAndCamera(liveInterrogation, true);
        }
      }
    }
  }

  private changeIsActiveToNvrAndCamera(liveInterogation: ActiveInterrogation, isActive) {
    for (const nvr of this.nvrs) {
      if (liveInterogation.nvr.nvrId === nvr.nvrId) {
        nvr.isActive = isActive;
        break;
      }
    }

    for (const camera of this.cameras) {
      if (liveInterogation.camera.cameraId === camera.cameraId) {
        camera.isActive = isActive;
        break;
      }
    }
  }

  public firstMenuClicked(event) {
    $('.collapse.show').collapse('toggle');
  }

  public changeWndNum(numOfScreens: number) {
    this.perviewCameraComponent.changeWndNum(numOfScreens);
  }

  public playVideo() {
    this.perviewCameraComponent.playVideo(this.selectedCamera, this.selectedNvr)
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.NVR_LOGIN_FAILED || videoPreviewMessage.status === VideoPerviewStatus.FAILED_PLAYING_VIDEO)
          this.toasterService.showError('Failed playing video');
      })
      .catch((err) => {
        this.toasterService.showError('Failed playing video');
      });
  }

  public stopVideo() {
    this.perviewCameraComponent.stopVideo()
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.FAILED_STOPING_VIDEO)
          this.toasterService.showError('Failed stoping video');
      })
      .catch((err) => {
        this.toasterService.showError('Failed stoping video');
      });
  }

  public openSound() {
    this.perviewCameraComponent.openSound()
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.FAILED_OPEN_SOUND)
          this.toasterService.showError('Failed open sound');
      })
      .catch((err) => {
        this.toasterService.showError('Failed open sound');
      });
  }

  public closeSound() {
    this.perviewCameraComponent.closeSound()
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.FAILED_CLOSE_SOUND)
          this.toasterService.showError('Failed close sound');
      })
      .catch((err) => {
        this.toasterService.showError('Failed close sound');
      });
  }

  public logout(nvr: Nvr) {
    this.perviewCameraComponent.logout(nvr)
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.NVR_LOGOUT_FAILED)
          this.toasterService.showError('NVR logut failed');
      })
      .catch((err) => {
        this.toasterService.showError('NVR logut failed');
      });
  }

}

