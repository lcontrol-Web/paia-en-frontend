import { UserGroupTypes } from './../../core/enums/user-group-types.enum';
import { UserGroupService } from './../user-groups/services/user-group.service';
import { UserGroup } from './../user-groups/models/user-group';
import { AuthService } from './../../core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { LoggedUser } from 'src/app/core/models/logged-user';
import { Pagination } from 'src/app/core/models/pagination';
import { SortAction } from 'src/app/core/models/sort-action';
import { TableColumn } from 'src/app/core/models/table-column';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import { User } from './models/user';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public userId: number;
  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public checkedAll: boolean = false;
  public users: User[] = [];
  public userGroups: UserGroup[] = [];
  public selectedUserGroup: UserGroup;
  public dataIsReady: boolean = false;
  public userGroupsIsReady: boolean = false;
  public showDeleteForm: boolean = false;
  public showUserForm: boolean = false;
  public itemsToDelete: User[] = [];
  public checkedUsers: User[] = [];
  public loggedUser: LoggedUser;

  public columns: TableColumn[] = [
    {
      name: 'user name',
      type: DataTypes.String,
      property: 'userName',
    },
  ];

  constructor(private userService: UserService,
    private userGroupService: UserGroupService,
    private utilsService: UtilsService,
    private authService: AuthService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentUserValue();
    if (this.loggedUser.userGroupType === UserGroupTypes.Administrator)
      this.getUserGroups();
    else {
      this.getUsers();
    }

  }

  private getUserGroups() {
    this.userGroupService.getUserGroups()
      .subscribe(
        (userGroups: UserGroup[]) => {
          this.userGroups = userGroups;
          this.userGroupsIsReady = true;
          if (this.userGroups.length > 0) {
            this.selectedUserGroup = this.userGroups[0];
            this.getUsers();
          }
          else
            this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  private getUsers() {
    let userGroupId: number =
      this.loggedUser.userGroupType === UserGroupTypes.Administrator ?
        this.selectedUserGroup.userGroupId :
        this.loggedUser.userGroupId;
    forkJoin([
      this.userService.getUsersByUserGroupIdAndPagination(userGroupId, this.pagination),
      this.userService.getAmountOfUsersByUserGroupIdAndPagination(userGroupId, this.pagination)
    ])
      .subscribe(
        (response: [User[], number]) => {
          this.users = response[0];
          this.total = response[1];
          this.setChecked();
          this.setCheckedAll();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  public userGroupChanged(userGroup: UserGroup) {
    this.dataIsReady = false;
    this.selectedUserGroup = userGroup;
    this.users = [];
    this.total = 0;
    this.getUsers();
  }

  public addUser() {
    this.showUserForm = true;
  }

  public updateUser(user: User) {
    this.showUserForm = true;
    this.userId = user.userId;
  }

  public onAddOrUpdateUser(user: User) {
    this.dataIsReady = false;
    this.resetUserForm();
    this.getUsers();
  }

  public resetUserForm() {
    this.userId = null;
    this.showUserForm = false;
  }


  public deleteUser(user: User) {
    this.itemsToDelete = [user];
    this.showDeleteForm = true;
  }

  public deleteMultipe() {
    this.itemsToDelete = [...this.checkedUsers];
    this.showDeleteForm = true;
  }

  public onDeleteForm(deletedUsers: User[]) {
    this.dataIsReady = false;
    this.checkedAll = false;

    // remove the deleted items from the checked items
    deletedUsers.forEach((deletedUser: User) => {
      const index = this.checkedUsers.findIndex(user => user.userId === deletedUser.userId);
      if (index >= 0)
        this.checkedUsers.splice(index, 1);
    });
    this.setPage();
    this.resetDeleteForm();
    this.getUsers();
  }

  public resetDeleteForm() {
    this.itemsToDelete = [];
    this.showDeleteForm = false;
  }

  private setPage() {
    if (this.users.length === 1 && this.pagination.page > 1)
      this.pagination.page = this.pagination.page - 1;
  }

  public onChecked(user: User, value: boolean) {
    user.checked = value;
    const index = this.checkedUsers.findIndex(item => item.userId === user.userId);

    if (index === -1 && user.checked) {
      this.checkedUsers.push(user);
      this.setCheckedAll();
    }
    else if (index > -1 && !user.checked) {
      this.checkedAll = false;
      this.checkedUsers.splice(index, 1);
    }
  }

  public onCheckedAll(checkedAll: boolean) {

    this.checkedAll = checkedAll;

    this.users.forEach((user: User) => {
      user.checked = this.checkedAll;
      const index = this.checkedUsers.findIndex(e => e.userId === user.userId);
      if (this.checkedAll && index === -1)
        this.checkedUsers.push(user);
      else if (!this.checkedAll && index >= 0)
        this.checkedUsers.splice(index, 1);
    });
  }

  private setChecked() {
    // check the item if the item exists in the checked array
    this.checkedUsers.forEach((checkedUser: User) => {
      const index = this.users.findIndex(e => e.userId === checkedUser.userId);
      if (index >= 0)
        this.users[index].checked = true;
    });
  }

  private setCheckedAll() {
    if (this.users.length === 0)
      this.checkedAll = false;
    else {
      const equal = this.utilsService.arraysAreEqual(this.checkedUsers, this.users, 'userId');
      equal ? this.checkedAll = true : this.checkedAll = false;
    }
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.page = page;
    this.getUsers();
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;

    this.getUsers();
  }


  public sortByColumn(sortAction: SortAction) {
    this.dataIsReady = false;
    this.pagination.sort = sortAction.sort;
    this.pagination.column = sortAction.column;
    this.getUsers();
  }

  public filterByColumn(filterAction: FilterAction) {
    this.dataIsReady = false;
    this.pagination.condition = filterAction.condition;
    this.pagination.column = filterAction.column;
    this.pagination.searchText = filterAction.searchText;
    this.getUsers();
  }

  public clearSortByColumn() {
    this.dataIsReady = false;
    this.pagination.sort = SortTypes.Descending;
    this.pagination.column = null;
    this.getUsers();
  }

  public clearFilterByColumn() {
    this.dataIsReady = false;
    this.pagination.condition = null;
    this.pagination.searchText = null;
    this.getUsers();
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public onSearch(searchValue: string) {
    this.pagination.page = 1;
    this.pagination.searchText = searchValue;
    this.pagination.condition = null;
    this.pagination.column = null;
    this.pagination.sort = SortTypes.Descending;
    this.checkedAll = false;
    this.getUsers();
  }
}
