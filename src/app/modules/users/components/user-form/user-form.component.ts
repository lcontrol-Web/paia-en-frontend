import { UserGroup } from './../../../user-groups/models/user-group';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Actions } from 'src/app/core/enums/actions.enum';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { UserGroupTypes } from 'src/app/core/enums/user-group-types.enum';
declare var $: any;

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit, OnDestroy {

  @Input() userId: number;
  @Input() userGroups: UserGroup[] = [];
  @Output('onAdd') private addEmitter: EventEmitter<User> = new EventEmitter();
  @Output('onUpdate') private updateEmitter: EventEmitter<User> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<any> = new EventEmitter();

  public action: Actions = Actions.Add;
  public isAddMode: boolean = true;
  public isLoading: boolean = false;
  public dataIsReady: boolean = false;
  public user: User = new User();
  public teamLeaders: User[] = [];
  public selectedTeamLeader: User = null;
  public selectedUserGroup: UserGroup = null;

  constructor(private userService: UserService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.checkMode();
    this.isAddMode ? this.dataIsReady = true : this.getUser();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#addUpdateModal').modal('show');
  }

  private closeModal() {
    $('#addUpdateModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private checkMode() {
    if (this.userId) {
      this.action = Actions.Update;
      this.isAddMode = false;
    }
  }

  private getUser() {
    this.userService.getUserByUserId(this.userId)
      .subscribe(
        (user: User) => {
          this.user = user;
          this.setUserGroup();
          if (this.selectedUserGroup.userGroupType === UserGroupTypes.Investigator)
            this.getTeamLeaders();

          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot get data');
        });
  }

  public userGroupChanged(userGroup: UserGroup) {
    this.selectedTeamLeader = null;
    if (this.selectedUserGroup.userGroupType === UserGroupTypes.Investigator) {
      this.dataIsReady = false;
      this.getTeamLeaders();
    }
  }

  private getTeamLeaders() {
    this.userService.getTeamLeaders()
      .subscribe(
        (teamLeaders: User[]) => {
          this.teamLeaders = teamLeaders;
          if (!this.isAddMode)
            this.setTeamLeader();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot get data');
        });
  }

  private setUserGroup() {
    this.selectedUserGroup = this.userGroups.find(userGroup => this.user.userGroupId === userGroup.userGroupId);
  }


  private setTeamLeader() {
    this.selectedTeamLeader = this.teamLeaders.find(teamLeader => this.user.teamLeaderId === teamLeader.userId);
  }


  public onSubmit() {
    this.isLoading = true;
    this.user.userGroupId = this.selectedUserGroup.userGroupId;
    if (this.selectedUserGroup.userGroupType === UserGroupTypes.Investigator)
      this.user.teamLeaderId = this.selectedTeamLeader.userId;

    const httpReq: Observable<User> = this.isAddMode ?
      this.userService.addUser(this.user) :
      this.userService.updateUser(this.userId, this.user);

    httpReq
      .subscribe(
        (user: User) => {
          this.closeModal();
          setTimeout(() => {
            this.isAddMode ? this.addEmitter.emit(user) : this.updateEmitter.emit(user);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          if (error === 'User name exists')
            this.toasterService.showError(error);
          else
            this.toasterService.showError('Error');
        });
  }
}

