import { PaginationService } from './../../../core/services/pagination.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pagination } from 'src/app/core/models/pagination';
import { DataService } from 'src/app/core/services/data.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiName = 'users';

  constructor(private dataService: DataService,
    private paginationService: PaginationService) { }

  public getUsers(): Observable<User[]> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getData();
  }
  
  public getTeamLeaders(): Observable<User[]> {
    this.dataService.apiName = `${this.apiName}/team-leaders` ;
    return this.dataService.getData();
  }

  public getUsersByPagination(pagination: Pagination): Observable<User[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName;
    return this.dataService.getData(params);
  }

  public getUsersByUserGroupIdAndPagination(userGroupId: number, pagination: Pagination): Observable<User[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = `user-groups/${userGroupId}/${this.apiName}`;
    return this.dataService.getData(params);
  }

  public getUserByUserId(userId: number): Observable<User> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getDataById(userId);
  }

  public getAmountOfUsers(): Observable<number> {
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData();
  }

  public getAmountOfUsersByUserGroupIdAndPagination(userGroupId: number, pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = `${this.apiName}/${userGroupId}/amount`;
    return this.dataService.getData(params);
  }

  public getAmountOfUsersByPagination(pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData(params);
  }

  public addUser(user: User): Observable<User> {
    user = this.deleteOptionalProperties(user);
    this.dataService.apiName = this.apiName;
    return this.dataService.add(user);
  }

  public updateUser(userId: number, user: User): Observable<User> {
    user = this.deleteOptionalProperties(user);
    this.dataService.apiName = this.apiName;
    return this.dataService.update(userId, user);
  }

  private deleteOptionalProperties(user: User): User {
    delete user.checked;
    delete user.teamLeaderName;
    return user;
  }
}
