export class User {
    public userId: number;
    public userName: string;
    public userGroupId: number;
    public teamLeaderId: number;
    public teamLeaderName?: string;
    public checked?: boolean = false;
}

