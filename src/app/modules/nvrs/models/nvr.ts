export class Nvr {
    public nvrId: number;
    public nvrName: string;
    public ip: string;
    public channels: number = 16;
    public userName: string;
    public password: string;
    public port: number = 80;
    public sdkPort: number = 8001;
    public rtspPort: number = 554;
    public checked?: boolean = false;
    public isActive?: boolean = false;
}
