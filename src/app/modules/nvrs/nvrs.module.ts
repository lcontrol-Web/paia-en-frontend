import { NvrFormComponent } from './components/nvr-form/nvr-form.component';
import { NgModule } from '@angular/core';
import { NvrsComponent } from './nvrs.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: NvrsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    NvrsComponent,
    NvrFormComponent
  ]
})
export class NvrsModule { }
