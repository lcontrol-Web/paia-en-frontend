import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { Pagination } from 'src/app/core/models/pagination';
import { SortAction } from 'src/app/core/models/sort-action';
import { TableColumn } from 'src/app/core/models/table-column';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import { Nvr } from './models/nvr';
import { NvrService } from './services/nvr.service';

@Component({
  selector: 'app-nvrs',
  templateUrl: './nvrs.component.html',
  styleUrls: ['./nvrs.component.css']
})
export class NvrsComponent implements OnInit {

  public nvrId: number;
  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public checkedAll: boolean = false;
  public nvrs: Nvr[] = [];
  public dataIsReady: boolean = false;
  public showDeleteForm: boolean = false;
  public showNvrForm: boolean = false;
  public itemsToDelete: Nvr[] = [];
  public checkedNvrs: Nvr[] = [];

  public columns: TableColumn[] = [
    {
      name: 'nvr name',
      type: DataTypes.String,
      property: 'nvrName',
    },
    {
      name: 'ip',
      type: DataTypes.String,
      property: 'ip',
    },
    {
      name: 'port',
      type: DataTypes.Number,
      property: 'port',
    },
    {
      name: 'sdkPort',
      type: DataTypes.Number,
      property: 'sdkPort',
    },
    {
      name: 'rtspPort',
      type: DataTypes.Number,
      property: 'rtspPort',
    },
    {
      name: 'channels',
      type: DataTypes.Number,
      property: 'channels',
    },
  ];

  constructor(private nvrService: NvrService,
    private utilsService: UtilsService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.getNvrs();
  }

  private getNvrs() {
    forkJoin([
      this.nvrService.getNvrsByPagination(this.pagination),
      this.nvrService.getAmountOfNvrsByPagination(this.pagination)
    ])
      .subscribe(
        (response: [Nvr[], number]) => {
          this.nvrs = response[0];
          this.total = response[1];
          this.setChecked();
          this.setCheckedAll();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  public addNvr() {
    this.showNvrForm = true;
  }

  public updateNvr(nvr: Nvr) {
    this.showNvrForm = true;
    this.nvrId = nvr.nvrId;
  }

  public onAddOrUpdateNvr(nvr: Nvr) {
    this.dataIsReady = false;
    this.resetNvrForm();
    this.getNvrs();
  }

  public resetNvrForm() {
    this.nvrId = null;
    this.showNvrForm = false;
  }


  public deleteNvr(nvr: Nvr) {
    this.itemsToDelete = [nvr];
    this.showDeleteForm = true;
  }

  public deleteMultipe() {
    this.itemsToDelete = [...this.checkedNvrs];
    this.showDeleteForm = true;
  }

  public onDeleteForm(deletedUsers: Nvr[]) {
    this.dataIsReady = false;
    this.checkedAll = false;

    // remove the deleted items from the checked items
    deletedUsers.forEach((deletedNvr: Nvr) => {
      const index = this.checkedNvrs.findIndex(nvr => nvr.nvrId === deletedNvr.nvrId);
      if (index >= 0)
        this.checkedNvrs.splice(index, 1);
    });
    this.setPage();
    this.resetDeleteForm();
    this.getNvrs();
  }

  public resetDeleteForm() {
    this.itemsToDelete = [];
    this.showDeleteForm = false;
  }

  private setPage() {
    if (this.nvrs.length === 1 && this.pagination.page > 1)
      this.pagination.page = this.pagination.page - 1;
  }

  public onChecked(nvr: Nvr, value: boolean) {
    nvr.checked = value;
    const index = this.checkedNvrs.findIndex(item => item.nvrId === nvr.nvrId);

    if (index === -1 && nvr.checked) {
      this.checkedNvrs.push(nvr);
      this.setCheckedAll();
    }
    else if (index > -1 && !nvr.checked) {
      this.checkedAll = false;
      this.checkedNvrs.splice(index, 1);
    }
  }

  public onCheckedAll(checkedAll: boolean) {

    this.checkedAll = checkedAll;

    this.nvrs.forEach((nvr: Nvr) => {
      nvr.checked = this.checkedAll;
      const index = this.checkedNvrs.findIndex(e => e.nvrId === nvr.nvrId);
      if (this.checkedAll && index === -1)
        this.checkedNvrs.push(nvr);
      else if (!this.checkedAll && index >= 0)
        this.checkedNvrs.splice(index, 1);
    });
  }

  private setChecked() {
    // check the item if the item exists in the checked array
    this.checkedNvrs.forEach((checkedNvr: Nvr) => {
      const index = this.nvrs.findIndex(e => e.nvrId === checkedNvr.nvrId);
      if (index >= 0)
        this.nvrs[index].checked = true;
    });
  }

  private setCheckedAll() {
    if (this.nvrs.length === 0)
      this.checkedAll = false;
    else {
      const equal = this.utilsService.arraysAreEqual(this.checkedNvrs, this.nvrs, 'nvrId');
      equal ? this.checkedAll = true : this.checkedAll = false;
    }
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.page = page;
    this.getNvrs();
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;

    this.getNvrs();
  }


  public sortByColumn(sortAction: SortAction) {
    this.dataIsReady = false;
    this.pagination.sort = sortAction.sort;
    this.pagination.column = sortAction.column;
    this.getNvrs();
  }

  public filterByColumn(filterAction: FilterAction) {
    this.dataIsReady = false;
    this.pagination.condition = filterAction.condition;
    this.pagination.column = filterAction.column;
    this.pagination.searchText = filterAction.searchText;
    this.getNvrs();
  }

  public clearSortByColumn() {
    this.dataIsReady = false;
    this.pagination.sort = SortTypes.Descending;
    this.pagination.column = null;
    this.getNvrs();
  }

  public clearFilterByColumn() {
    this.dataIsReady = false;
    this.pagination.condition = null;
    this.pagination.searchText = null;
    this.getNvrs();
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public onSearch(searchValue: string) {
    this.pagination.page = 1;
    this.pagination.searchText = searchValue;
    this.pagination.condition = null;
    this.pagination.column = null;
    this.pagination.sort = SortTypes.Descending;
    this.checkedAll = false;
    this.getNvrs();
  }
}
