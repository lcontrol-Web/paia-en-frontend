import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Actions } from 'src/app/core/enums/actions.enum';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { Nvr } from '../../models/nvr';
import { NvrService } from '../../services/nvr.service';
declare var $: any;

@Component({
  selector: 'app-nvr-form',
  templateUrl: './nvr-form.component.html',
  styleUrls: ['./nvr-form.component.css']
})
export class NvrFormComponent implements OnInit, OnDestroy {

  @Input() nvrId: number;
  @Output('onAdd') private addEmitter: EventEmitter<Nvr> = new EventEmitter();
  @Output('onUpdate') private updateEmitter: EventEmitter<Nvr> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<any> = new EventEmitter();

  public action: Actions = Actions.Add;
  public isAddMode: boolean = true;
  public isLoading: boolean = false;
  public dataIsReady: boolean = false;
  public showPassword: boolean = false;
  public ipError: boolean = false;
  public nvr: Nvr = new Nvr();

  constructor(private nvrService: NvrService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.checkMode();
    this.isAddMode ? this.dataIsReady = true : this.getNvr();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#addUpdateModal').modal('show');
  }

  private closeModal() {
    $('#addUpdateModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private checkMode() {
    if (this.nvrId) {
      this.action = Actions.Update;
      this.isAddMode = false;
    }
  }

  private getNvr() {
    this.nvrService.getNvrByNvrId(this.nvrId)
      .subscribe(
        (nvr: Nvr) => {
          this.nvr = nvr;
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot get data');
        });
  }

  public ipChanged(nvr: Nvr) {
    const regex = /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/;
    if (regex.test(this.nvr.ip)) {
      this.ipError = false;
    }
    else
      this.ipError = true;
  }

  public onSubmit() {
    this.isLoading = true;
    const httpReq: Observable<Nvr> = this.isAddMode ?
      this.nvrService.addNvr(this.nvr) :
      this.nvrService.updateNvr(this.nvrId, this.nvr);

    httpReq
      .subscribe(
        (nvr: Nvr) => {
          this.closeModal();
          setTimeout(() => {
            this.isAddMode ? this.addEmitter.emit(nvr) : this.updateEmitter.emit(nvr);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Error');
        });
  }
}

