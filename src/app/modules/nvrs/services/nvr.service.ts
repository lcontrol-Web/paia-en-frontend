import { PaginationService } from './../../../core/services/pagination.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pagination } from 'src/app/core/models/pagination';
import { DataService } from 'src/app/core/services/data.service';
import { Nvr } from '../models/nvr';

@Injectable({
  providedIn: 'root'
})
export class NvrService {

  private apiName = 'nvrs';

  constructor(private dataService: DataService,
    private paginationService: PaginationService) { }

  public getNvrs(): Observable<Nvr[]> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getData();
  }

  public getNvrsByPagination(pagination: Pagination): Observable<Nvr[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName;
    return this.dataService.getData(params);
  }

  public getNvrByNvrId(nvrId: number): Observable<Nvr> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getDataById(nvrId);
  }

  public getAmountOfNvrs(): Observable<number> {
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData();
  }

  public getAmountOfNvrsByPagination(pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData(params);
  }

  public addNvr(nvr: Nvr): Observable<Nvr> {
    nvr = this.deleteOptionalProperties(nvr);
    this.dataService.apiName = this.apiName;
    return this.dataService.add(nvr);
  }

  public updateNvr(nvrId: number, nvr: Nvr): Observable<Nvr> {
    nvr = this.deleteOptionalProperties(nvr);
    this.dataService.apiName = this.apiName;
    return this.dataService.update(nvrId, nvr);
  }

  private deleteOptionalProperties(nvr: Nvr): Nvr {
    delete nvr.checked;
    delete nvr.isActive;
    return nvr;
  }
}