import { UtilsService } from './../../core/services/utils.service';
import { TableColumn } from './../../core/models/table-column';
import { ToasterService } from './../../core/services/toaster.service';
import { Component, OnInit } from '@angular/core';
import { UserGroup } from './models/user-group';
import { UserGroupService } from './services/user-group.service';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { Pagination } from 'src/app/core/models/pagination';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { SortAction } from 'src/app/core/models/sort-action';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.css'],
})
export class UserGroupsComponent implements OnInit {

  public userGroupId: number;
  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public checkedAll: boolean = false;
  public userGroups: UserGroup[] = [];
  public dataIsReady: boolean = false;
  public showDeleteForm: boolean = false;
  public showUserGroupForm: boolean = false;
  public itemsToDelete: UserGroup[] = [];
  public checkedUserGroups: UserGroup[] = [];

  public columns: TableColumn[] = [
    {
      name: 'group name',
      type: DataTypes.String,
      property: 'userGroupName',
    },
    {
      name: 'group type',
      type: DataTypes.String,
      property: 'userGroupType',
    },
  ];

  constructor(private userGroupService: UserGroupService,
    private utilsService: UtilsService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.getUserGroups();
  }

  private getUserGroups() {
    forkJoin([
      this.userGroupService.getUserGroupsByPagination(this.pagination),
      this.userGroupService.getAmountOfUserGroupsByPagination(this.pagination)
    ])
      .subscribe(
        (response: [UserGroup[], number]) => {
          this.userGroups = response[0];
          this.total = response[1];
          this.setChecked();
          this.setCheckedAll();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  public addUserGroup() {
    this.showUserGroupForm = true;
  }

  public updateUserGroup(userGroup: UserGroup) {
    this.showUserGroupForm = true;
    this.userGroupId = userGroup.userGroupId;
  }

  public onAddOrUpdateUserGroup(userGroup: UserGroup) {
    this.dataIsReady = false;
    this.resetUserGroupForm();
    this.getUserGroups();
  }

  public resetUserGroupForm() {
    this.userGroupId = null;
    this.showUserGroupForm = false;
  }


  public deleteUserGroup(userGroup: UserGroup) {
    this.itemsToDelete = [userGroup];
    this.showDeleteForm = true;
  }

  public deleteMultipe() {
    this.itemsToDelete = [...this.checkedUserGroups];
    this.showDeleteForm = true;
  }

  public onDeleteForm(deletedUsers: UserGroup[]) {
    this.dataIsReady = false;
    this.checkedAll = false;

    // remove the deleted items from the checked items
    deletedUsers.forEach((deletedUserGroup: UserGroup) => {
      const index = this.checkedUserGroups.findIndex(userGroup => userGroup.userGroupId === deletedUserGroup.userGroupId);
      if (index >= 0)
        this.checkedUserGroups.splice(index, 1);
    });
    this.setPage();
    this.resetDeleteForm();
    this.getUserGroups();
  }

  public resetDeleteForm() {
    this.itemsToDelete = [];
    this.showDeleteForm = false;
  }

  private setPage() {
    if (this.userGroups.length === 1 && this.pagination.page > 1)
      this.pagination.page = this.pagination.page - 1;
  }

  public onChecked(userGroup: UserGroup, value: boolean) {
    userGroup.checked = value;
    const index = this.checkedUserGroups.findIndex(item => item.userGroupId === userGroup.userGroupId);

    if (index === -1 && userGroup.checked) {
      this.checkedUserGroups.push(userGroup);
      this.setCheckedAll();
    }
    else if (index > -1 && !userGroup.checked) {
      this.checkedAll = false;
      this.checkedUserGroups.splice(index, 1);
    }
  }

  public onCheckedAll(checkedAll: boolean) {

    this.checkedAll = checkedAll;

    this.userGroups.forEach((userGroup: UserGroup) => {
      userGroup.checked = this.checkedAll;
      const index = this.checkedUserGroups.findIndex(e => e.userGroupId === userGroup.userGroupId);
      if (this.checkedAll && index === -1)
        this.checkedUserGroups.push(userGroup);
      else if (!this.checkedAll && index >= 0)
        this.checkedUserGroups.splice(index, 1);
    });
  }

  private setChecked() {
    // check the item if the item exists in the checked array
    this.checkedUserGroups.forEach((checkedUserGroup: UserGroup) => {
      const index = this.userGroups.findIndex(e => e.userGroupId === checkedUserGroup.userGroupId);
      if (index >= 0)
        this.userGroups[index].checked = true;
    });
  }

  private setCheckedAll() {
    if (this.userGroups.length === 0)
      this.checkedAll = false;
    else {
      const equal = this.utilsService.arraysAreEqual(this.checkedUserGroups, this.userGroups, 'userGroupId');
      equal ? this.checkedAll = true : this.checkedAll = false;
    }
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.page = page;
    this.getUserGroups();
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;

    this.getUserGroups();
  }


  public sortByColumn(sortAction: SortAction) {
    this.dataIsReady = false;
    this.pagination.sort = sortAction.sort;
    this.pagination.column = sortAction.column;
    this.getUserGroups();
  }

  public filterByColumn(filterAction: FilterAction) {
    this.dataIsReady = false;
    this.pagination.condition = filterAction.condition;
    this.pagination.column = filterAction.column;
    this.pagination.searchText = filterAction.searchText;
    this.getUserGroups();
  }

  public clearSortByColumn() {
    this.dataIsReady = false;
    this.pagination.sort = SortTypes.Descending;
    this.pagination.column = null;
    this.getUserGroups();
  }

  public clearFilterByColumn() {
    this.dataIsReady = false;
    this.pagination.condition = null;
    this.pagination.searchText = null;
    this.getUserGroups();
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public onSearch(searchValue: string) {
    this.pagination.page = 1;
    this.pagination.searchText = searchValue;
    this.pagination.condition = null;
    this.pagination.column = null;
    this.pagination.sort = SortTypes.Descending;
    this.checkedAll = false;
    this.getUserGroups();
  }
}
