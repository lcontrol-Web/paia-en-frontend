import { UserGroupTypes } from './../../../core/enums/user-group-types.enum';
export class UserGroup {
    public userGroupId: number;
    public userGroupName: string;
    public userGroupType: UserGroupTypes = null;
    public checked: boolean = false;
}