import { UserGroupFormComponent } from './components/user-group-form/user-group-form.component';
import { NgModule } from '@angular/core';
import { UserGroupsComponent } from './user-groups.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: UserGroupsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,

  ],
  declarations: [
    UserGroupsComponent,
    UserGroupFormComponent
  ]
})
export class UserGroupsModule { }
