import { PaginationService } from './../../../core/services/pagination.service';
import { Pagination } from './../../../core/models/pagination';
import { DataService } from './../../../core/services/data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { UserGroup } from '../models/user-group';

@Injectable({
  providedIn: 'root'
})
export class UserGroupService {

  private apiName = 'user-groups';

  constructor(private dataService: DataService,
    private paginationService: PaginationService) { }

  public getUserGroups(): Observable<UserGroup[]> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getData();
  }

  public getUserGroupsByPagination(pagination: Pagination): Observable<UserGroup[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName;
    return this.dataService.getData(params);
  }

  public getUserGroupByUserGroupId(userGroupId: number): Observable<UserGroup> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getDataById(userGroupId);
  }

  public getAmountOfUserGroups(): Observable<number> {
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData();
  }

  public getAmountOfUserGroupsByPagination(pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData(params);
  }

  public addUserGroup(userGroup: UserGroup): Observable<UserGroup> {
    userGroup = this.deleteOptionalProperties(userGroup);
    this.dataService.apiName = this.apiName;
    return this.dataService.add(userGroup);
  }

  public updateUserGroup(userGroupId: number, userGroup: UserGroup): Observable<UserGroup> {
    userGroup = this.deleteOptionalProperties(userGroup);
    this.dataService.apiName = this.apiName;
    return this.dataService.update(userGroupId, userGroup);
  }

  private deleteOptionalProperties(userGroup: UserGroup): UserGroup {
    delete userGroup.checked;
    return userGroup;
  }
}