import { UserGroupTypes } from './../../../../core/enums/user-group-types.enum';
import { ToasterService } from './../../../../core/services/toaster.service';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Actions } from 'src/app/core/enums/actions.enum';
import { UserGroup } from '../../models/user-group';
import { UserGroupService } from '../../services/user-group.service';
declare var $: any;

@Component({
  selector: 'app-user-group-form',
  templateUrl: './user-group-form.component.html',
  styleUrls: ['./user-group-form.component.css']
})
export class UserGroupFormComponent implements OnInit, OnDestroy {

  @Input() userGroupId: number;
  @Output('onAdd') private addEmitter: EventEmitter<UserGroup> = new EventEmitter();
  @Output('onUpdate') private updateEmitter: EventEmitter<UserGroup> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<any> = new EventEmitter();

  public action: Actions = Actions.Add;
  public isAddMode: boolean = true;
  public isLoading: boolean = false;
  public dataIsReady: boolean = false;
  public userGroup: UserGroup = new UserGroup();
  public userGroupTypes: UserGroupTypes[] = [
    UserGroupTypes.Administrator,
    UserGroupTypes.TeamLeader,
    UserGroupTypes.Manager,
    UserGroupTypes.Investigator,
    UserGroupTypes.NoPermission,
  ];

  constructor(private userGroupService: UserGroupService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.checkMode();
    this.isAddMode ? this.dataIsReady = true : this.getUserGroup();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#addUpdateModal').modal('show');
  }

  private closeModal() {
    $('#addUpdateModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private checkMode() {
    if (this.userGroupId) {
      this.action = Actions.Update;
      this.isAddMode = false;
    }
  }

  private getUserGroup() {
    this.userGroupService.getUserGroupByUserGroupId(this.userGroupId)
      .subscribe(
        (userGroup: UserGroup) => {
          this.userGroup = userGroup;
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot get data');
        });
  }

  public onSubmit() {
    this.isLoading = true;
    const httpReq: Observable<UserGroup> = this.isAddMode ?
      this.userGroupService.addUserGroup(this.userGroup) :
      this.userGroupService.updateUserGroup(this.userGroupId, this.userGroup);

    httpReq
      .subscribe(
        (userGroup: UserGroup) => {
          this.closeModal();
          setTimeout(() => {
            this.isAddMode ? this.addEmitter.emit(userGroup) : this.updateEmitter.emit(userGroup);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Error');
        });
  }
}

