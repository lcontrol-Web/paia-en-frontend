export class Camera {
    public cameraId: number;
    public nvrId: number;
    public cameraName: string;
    public channel: number = null;
    public cameraIp: string;
    public controllerIp: string;
    public indexArray: number = 17;
    public checked?: boolean = false;
    public isActive?: boolean = false;
}
