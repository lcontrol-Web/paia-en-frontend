import { CameraFormComponent } from './components/camera-form/camera-form.component';
import { NgModule } from '@angular/core';
import { CamerasComponent } from './cameras.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: CamerasComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    CamerasComponent,
    CameraFormComponent
  ]
})
export class CamerasModule { }
