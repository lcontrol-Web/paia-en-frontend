import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Actions } from 'src/app/core/enums/actions.enum';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { Nvr } from 'src/app/modules/nvrs/models/nvr';
import { Camera } from '../../models/camera';
import { CameraService } from '../../services/camera.service';
declare var $: any;

@Component({
  selector: 'app-camera-form',
  templateUrl: './camera-form.component.html',
  styleUrls: ['./camera-form.component.css']
})
export class CameraFormComponent implements OnInit, OnDestroy {

  @Input() cameraId: number;
  @Input() nvrs: Nvr[] = [];
  @Input() cameras: Camera[] = [];
  @Output('onAdd') private addEmitter: EventEmitter<Camera> = new EventEmitter();
  @Output('onUpdate') private updateEmitter: EventEmitter<Camera> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<any> = new EventEmitter();

  public action: Actions = Actions.Add;
  public isAddMode: boolean = true;
  public isLoading: boolean = false;
  public dataIsReady: boolean = false;
  public cameraIpError: boolean = false;
  public controllerIpError: boolean = false;
  public camera: Camera = new Camera();
  public selectedNvr: Nvr = null;
  public channels: number[] = [];

  constructor(private cameraService: CameraService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.checkMode();
    this.isAddMode ? this.dataIsReady = true : this.getCamera();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#addUpdateModal').modal('show');
  }

  private closeModal() {
    $('#addUpdateModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private checkMode() {
    if (this.cameraId) {
      this.action = Actions.Update;
      this.isAddMode = false;
    }
  }

  private getCamera() {
    this.cameraService.getCameraByCameraId(this.cameraId)
      .subscribe(
        (camera: Camera) => {
          this.camera = camera;
          this.setNvr();
          this.setChannels();

          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot get data');
        });
  }

  private setNvr() {
    this.selectedNvr = this.nvrs.find(nvr => this.camera.nvrId === nvr.nvrId);
  }

  public cameraIpChanged(camera: Camera) {
    this.cameraIpError = this.validateIp(this.camera.cameraIp);
  }

  public controllerIpChanged(camrea: Camera) {
    this.controllerIpError = this.validateIp(this.camera.controllerIp);
  }

  private validateIp(ip: string): boolean {
    const regex = /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/;
    if (regex.test(ip))
      return false;

    return true;
  }

  public nvrChanged() {
    this.channels = [];
    this.camera.channel = null;
    this.setChannels();
    // this.camera.channel = this.getFreeChannel();
  }

  private setChannels(){
    for (let i = 0; i < this.selectedNvr.channels; i++)
      this.channels.push(i + 1);
  }


  private getFreeChannel() {

    // let takenChannels: number[] = this.cameras.map((camera: Camera) => {
    //   if (camera.nvrId === this.selectedNvr.nvrId)
    //     return camera.channel;
    // });

    // for (let i = 0; i < takenChannels.length; i++) {
    //   if (this.camera && takenChannels[i] === this.camera.channel) {
    //     continue;
    //   }
    //   else {
    //     const index = this.channels.indexOf(takenChannels[i]);
    //     if (index > -1) {
    //       this.channels.splice(index, 1);
    //     }
    //   }
    // }

    // if (this.channels.length > 0)
    //   return this.channels[0];
    // else
    //   return null;
  }


  public onSubmit() {
    this.isLoading = true;
    this.camera.nvrId = this.selectedNvr.nvrId;

    const httpReq: Observable<Camera> = this.isAddMode ?
      this.cameraService.addCamera(this.camera) :
      this.cameraService.updateCamera(this.cameraId, this.camera);

    httpReq
      .subscribe(
        (camera: Camera) => {
          this.closeModal();
          setTimeout(() => {
            this.isAddMode ? this.addEmitter.emit(camera) : this.updateEmitter.emit(camera);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Error');
        });
  }
}

