import { NvrService } from './../nvrs/services/nvr.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { Pagination } from 'src/app/core/models/pagination';
import { SortAction } from 'src/app/core/models/sort-action';
import { TableColumn } from 'src/app/core/models/table-column';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import { Nvr } from '../nvrs/models/nvr';
import { Camera } from './models/camera';
import { CameraService } from './services/camera.service';

@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.css']
})
export class CamerasComponent implements OnInit {

  public cameraId: number;
  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public checkedAll: boolean = false;
  public cameras: Camera[] = [];
  public nvrs: Nvr[] = [];
  public selectedNvr: Nvr;
  public dataIsReady: boolean = false;
  public nvrsIsReady: boolean = false;
  public showDeleteForm: boolean = false;
  public showCameraForm: boolean = false;
  public itemsToDelete: Camera[] = [];
  public checkedCameras: Camera[] = [];

  public columns: TableColumn[] = [
    {
      name: 'camera name',
      type: DataTypes.String,
      property: 'cameraName',
    },
    {
      name: 'channel',
      type: DataTypes.Number,
      property: 'channel',
    },
    {
      name: 'camera IP',
      type: DataTypes.String,
      property: 'cameraIp',
    },
    {
      name: 'controller IP',
      type: DataTypes.String,
      property: 'controllerIp',
    },

  ];

  constructor(private cameraService: CameraService,
    private nvrService: NvrService,
    private utilsService: UtilsService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.getNvrs();
  }

  private getNvrs() {
    this.nvrService.getNvrs()
      .subscribe(
        (nvrs: Nvr[]) => {
          this.nvrs = nvrs;
          this.nvrsIsReady = true;
          if (this.nvrs.length > 0) {
            this.selectedNvr = this.nvrs[0];
            this.getCameras();
          }
          else
            this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  private getCameras() {
    forkJoin([
      this.cameraService.getCamerasByNvrIdAndPagination(this.selectedNvr.nvrId, this.pagination),
      this.cameraService.getAmountOfCamerasByNvrIdAndPagination(this.selectedNvr.nvrId, this.pagination)
    ])
      .subscribe(
        (response: [Camera[], number]) => {
          this.cameras = response[0];
          this.total = response[1];
          this.setChecked();
          this.setCheckedAll();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  public nvrChanged(nvr: Nvr) {
    this.dataIsReady = false;
    this.selectedNvr = nvr;
    this.cameras = [];
    this.total = 0;
    this.getCameras();
  }

  public addCamera() {
    this.showCameraForm = true;
  }

  public updateCamera(camera: Camera) {
    this.showCameraForm = true;
    this.cameraId = camera.cameraId;
  }

  public onAddOrUpdateCamera(camera: Camera) {
    this.dataIsReady = false;
    this.resetCameraForm();
    this.getCameras();
  }

  public resetCameraForm() {
    this.cameraId = null;
    this.showCameraForm = false;
  }


  public deleteCamera(camera: Camera) {
    this.itemsToDelete = [camera];
    this.showDeleteForm = true;
  }

  public deleteMultipe() {
    this.itemsToDelete = [...this.checkedCameras];
    this.showDeleteForm = true;
  }

  public onDeleteForm(deletedUsers: Camera[]) {
    this.dataIsReady = false;
    this.checkedAll = false;

    // remove the deleted items from the checked items
    deletedUsers.forEach((deletedCamera: Camera) => {
      const index = this.checkedCameras.findIndex(camera => camera.cameraId === deletedCamera.cameraId);
      if (index >= 0)
        this.checkedCameras.splice(index, 1);
    });
    this.setPage();
    this.resetDeleteForm();
    this.getCameras();
  }

  public resetDeleteForm() {
    this.itemsToDelete = [];
    this.showDeleteForm = false;
  }

  private setPage() {
    if (this.cameras.length === 1 && this.pagination.page > 1)
      this.pagination.page = this.pagination.page - 1;
  }

  public onChecked(camera: Camera, value: boolean) {
    camera.checked = value;
    const index = this.checkedCameras.findIndex(item => item.cameraId === camera.cameraId);

    if (index === -1 && camera.checked) {
      this.checkedCameras.push(camera);
      this.setCheckedAll();
    }
    else if (index > -1 && !camera.checked) {
      this.checkedAll = false;
      this.checkedCameras.splice(index, 1);
    }
  }

  public onCheckedAll(checkedAll: boolean) {

    this.checkedAll = checkedAll;

    this.cameras.forEach((camera: Camera) => {
      camera.checked = this.checkedAll;
      const index = this.checkedCameras.findIndex(e => e.cameraId === camera.cameraId);
      if (this.checkedAll && index === -1)
        this.checkedCameras.push(camera);
      else if (!this.checkedAll && index >= 0)
        this.checkedCameras.splice(index, 1);
    });
  }

  private setChecked() {
    // check the item if the item exists in the checked array
    this.checkedCameras.forEach((checkedCamera: Camera) => {
      const index = this.cameras.findIndex(e => e.cameraId === checkedCamera.cameraId);
      if (index >= 0)
        this.cameras[index].checked = true;
    });
  }

  private setCheckedAll() {
    if (this.cameras.length === 0)
      this.checkedAll = false;
    else {
      const equal = this.utilsService.arraysAreEqual(this.checkedCameras, this.cameras, 'cameraId');
      equal ? this.checkedAll = true : this.checkedAll = false;
    }
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.page = page;
    this.getCameras();
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.dataIsReady = false;
    this.checkedAll = false;
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;

    this.getCameras();
  }


  public sortByColumn(sortAction: SortAction) {
    this.dataIsReady = false;
    this.pagination.sort = sortAction.sort;
    this.pagination.column = sortAction.column;
    this.getCameras();
  }

  public filterByColumn(filterAction: FilterAction) {
    this.dataIsReady = false;
    this.pagination.condition = filterAction.condition;
    this.pagination.column = filterAction.column;
    this.pagination.searchText = filterAction.searchText;
    this.getCameras();
  }

  public clearSortByColumn() {
    this.dataIsReady = false;
    this.pagination.sort = SortTypes.Descending;
    this.pagination.column = null;
    this.getCameras();
  }

  public clearFilterByColumn() {
    this.dataIsReady = false;
    this.pagination.condition = null;
    this.pagination.searchText = null;
    this.getCameras();
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public onSearch(searchValue: string) {
    this.pagination.page = 1;
    this.pagination.searchText = searchValue;
    this.pagination.condition = null;
    this.pagination.column = null;
    this.pagination.sort = SortTypes.Descending;
    this.checkedAll = false;
    this.getCameras();
  }
}
