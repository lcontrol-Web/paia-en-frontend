import { PaginationService } from './../../../core/services/pagination.service';
import { Pagination } from './../../../core/models/pagination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/core/services/data.service';
import { Camera } from '../models/camera';

@Injectable({
  providedIn: 'root'
})
export class CameraService {

  private apiName = 'cameras';

  constructor(private dataService: DataService,
    private paginationService: PaginationService) { }

  public getCameras(): Observable<Camera[]> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getData();
  }

  public getCamerasByPagination(pagination: Pagination): Observable<Camera[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName;
    return this.dataService.getData(params);
  }

  public getCamerasByNvrId(nvrId): Observable<Camera[]> {
    this.dataService.apiName = `nvrs/${nvrId}/${this.apiName}`;
    return this.dataService.getData();
  }

  public getCamerasByNvrIdAndPagination(nvrId, pagination: Pagination): Observable<Camera[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = `nvrs/${nvrId}/${this.apiName}`;
    return this.dataService.getData(params);
  }

  public getCameraByCameraId(cameraId: number): Observable<Camera> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getDataById(cameraId);
  }

  public getAmountOfCameras(): Observable<number> {
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData();
  }

  public getAmountOfCamerasByPagination(pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName + '/amount';
    return this.dataService.getData(params);
  }

  public getAmountOfCamerasByNvrIdAndPagination(nvrId: number, pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = `${this.apiName}/${nvrId}/amount`;
    return this.dataService.getData(params);
  }

  public addCamera(camera: Camera): Observable<Camera> {
    camera = this.deleteOptionalProperties(camera);
    this.dataService.apiName = this.apiName;
    return this.dataService.add(camera);
  }

  public updateCamera(cameraId: number, camera: Camera): Observable<Camera> {
    camera = this.deleteOptionalProperties(camera);
    this.dataService.apiName = this.apiName;
    return this.dataService.update(cameraId, camera);
  }

  private deleteOptionalProperties(camera: Camera): Camera {
    delete camera.checked;
    delete camera.isActive;
    return camera;
  }
}