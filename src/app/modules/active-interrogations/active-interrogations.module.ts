import { CloseActiveInterrogationModalComponent } from './components/close-active-interrogation-modal/close-active-interrogation-modal.component';
import { ActiveInterrogationModalComponent } from './components/active-interrogation-modal/active-interrogation-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActiveInterrogationsComponent } from './active-interrogations.component';

const routes: Routes = [
  {
    path: '',
    component: ActiveInterrogationsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    ActiveInterrogationsComponent,
    ActiveInterrogationModalComponent,
    CloseActiveInterrogationModalComponent
  ]
})
export class ActiveInterrogationsModule { }
