import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { io } from 'socket.io-client';
import { UserGroupTypes } from 'src/app/core/enums/user-group-types.enum';
import { ChatSocketMessage } from 'src/app/core/models/chat-socket-message';
import { LoggedUser } from 'src/app/core/models/logged-user';
import { Pagination } from 'src/app/core/models/pagination';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { environment } from 'src/environments/environment.prod';
import { ActiveInterrogation } from '../interrogations/models/active-interrogation';
import { User } from '../users/models/user';
import { UserService } from '../users/services/user.service';
import { ActiveInterrogationModalComponent } from './components/active-interrogation-modal/active-interrogation-modal.component';

@Component({
  selector: 'app-active-interrogations',
  templateUrl: './active-interrogations.component.html',
  styleUrls: ['./active-interrogations.component.css']
})
export class ActiveInterrogationsComponent implements OnInit, OnDestroy {

  @ViewChild(ActiveInterrogationModalComponent, { static: false }) liveInterrogationModalComponent: ActiveInterrogationModalComponent;

  public dataIsReady: boolean = false;
  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public showModal: boolean = false;
  public showCloseActiveInterrogationModal: boolean = false;
  public activeInterrogations: ActiveInterrogation[] = [];
  public socketDisconnected: boolean = false;
  public selectedActiveInterrogation: ActiveInterrogation;
  public socket;
  private users: User[] = [];
  private loggedUser: LoggedUser;

  constructor(private userService: UserService,
    private toasterService: ToasterService,
    private authService: AuthService) {
    this.loggedUser = this.authService.getCurrentUserValue();
  }

  ngOnInit() {
    if (this.loggedUser.userGroupType === UserGroupTypes.TeamLeader)
      this.getUsers();
    else {
      this.setupSocketConnection();
      this.dataIsReady = true;
    }
  }

  ngOnDestroy() {
    this.disconnect();
  }

  private getUsers() {
    this.userService.getUsers()
      .subscribe(
        (users: User[]) => {
          this.users = users;
          this.setupSocketConnection();
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('אנא נסה שנית במועד מאוחר יותר');
        });
  }

  private setupSocketConnection() {
    this.socket = io(environment.socketUrl, {
      query: {
        clientType: 1
      }
    });

    this.socket.on('connect', () => {
      console.log('Connected to socket');
    });

    this.socket.on('message', (message: ChatSocketMessage) => {
      // check if this is not our socket send the message
      if (message.from != this.socket.id)
        this.liveInterrogationModalComponent.addMessage(true, message.text, message.userName, message.userId);
    });

    this.socket.on('connected-to-room', (roomName: string) => {
      console.log('You are connected to room: ' + roomName);
    });

    this.socket.on('live-interrogations', (activeInterrogations: ActiveInterrogation[]) => {
      this.setActiveInterrigations(activeInterrogations);
    });

    this.socket.on('connect_error', (err) => {

    });

    this.socket.on('error', (err) => {
      console.log(err);
    });

    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });
  }

  public openModal(liveInterrogation: any) {
    this.selectedActiveInterrogation = liveInterrogation;
    this.showModal = true;
  }

  public openCloseInterrogationModal(liveInterrogation: ActiveInterrogation) {
    this.selectedActiveInterrogation = liveInterrogation;
    this.showCloseActiveInterrogationModal = true;
  }

  public pageChanged(page: number) {
    this.pagination.page = page;
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;
  }

  public onCloseModal() {
    setTimeout(() => {
      this.showModal = false;
      this.socketDisconnected = false;
    }, 200);
  }

  public onCloseCloseActiveInterrogationModal() {
    setTimeout(() => {
      this.showCloseActiveInterrogationModal = false;
    }, 200);
  }

  private disconnect() {
    if (this.socket)
      this.socket.disconnect();
  }

  private setActiveInterrigations(activeInterrogations: ActiveInterrogation[]) {

    // remove the old sockets
    for (let i = 0; i < this.activeInterrogations.length; i++) {
      if (!activeInterrogations.some(li => li.socketId === this.activeInterrogations[i].socketId)) {

        // check if the selected client is disconnected
        if (this.selectedActiveInterrogation && this.activeInterrogations[i].socketId === this.selectedActiveInterrogation.socketId)
          this.socketDisconnected = true;

        this.activeInterrogations.splice(i, 1);
      }
    }

    // add the new sockets
    for (const liveInterrogation of activeInterrogations) {
      if (!this.activeInterrogations.some(li => li.socketId === liveInterrogation.socketId)) {

        // show only the live interrogations that the team leader can see (by his users) or level 5
        if (this.loggedUser.userGroupType === UserGroupTypes.TeamLeader && this.users.some((user: User) => user.userId === liveInterrogation.userId) || this.loggedUser.userGroupType === UserGroupTypes.Administrator)
          this.activeInterrogations.push(liveInterrogation);
      }
    }
  }

}
