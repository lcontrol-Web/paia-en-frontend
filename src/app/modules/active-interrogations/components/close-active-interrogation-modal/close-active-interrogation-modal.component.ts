import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { ActiveInterrogation } from 'src/app/modules/interrogations/models/active-interrogation';
import { InterrogationService } from 'src/app/modules/interrogations/services/interrogation.service';
declare var $: any;

@Component({
  selector: 'app-close-active-interrogation-modal',
  templateUrl: './close-active-interrogation-modal.component.html',
  styleUrls: ['./close-active-interrogation-modal.component.css']
})
export class CloseActiveInterrogationModalComponent implements OnInit, OnDestroy {

  @Input() activeInterrogation: ActiveInterrogation;
  @Output('onStop') private stopEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public isLoading: boolean = false;

  constructor(private toasterService: ToasterService,
    private interrogationService: InterrogationService) { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#closeActiveInterrogationModal').modal('show');
  }

  private closeModal() {
    $('#closeActiveInterrogationModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }


  public closeActiveInterrogation() {
    this.isLoading = true;
    this.interrogationService.closeActiveInterrogation(this.activeInterrogation.socketId)
      .subscribe(
        (response: any) => {
          if (response.status) {
            this.toasterService.showSuccess('Active interrogation stoped.');
            this.closeModal();
            setTimeout(() => {
              this.stopEmitter.emit(true);
            }, 200);
          }
          else {
            this.toasterService.showError('Cannot stop active interrogation.');
            this.isLoading = false;
          }
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Cannot stop active interrogation.');
        });
  }
}