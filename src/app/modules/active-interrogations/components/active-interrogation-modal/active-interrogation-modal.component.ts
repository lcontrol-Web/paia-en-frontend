import { Component, EventEmitter, Input, OnInit, Output, ViewChild, OnDestroy } from '@angular/core';
import { VideoPerviewStatus } from 'src/app/core/enums/video-perview-status.enum';
import { ChatMessage } from 'src/app/core/models/chat-message';
import { ChatSocketMessage } from 'src/app/core/models/chat-socket-message';
import { VideoPreviewMessage } from 'src/app/core/models/video-preview-message';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { ActiveInterrogation } from 'src/app/modules/interrogations/models/active-interrogation';
import { PerviewCameraComponent } from 'src/app/shared/components/perview-camera/perview-camera.component';
declare var $: any;

@Component({
  selector: 'app-active-interrogation-modal',
  templateUrl: './active-interrogation-modal.component.html',
  styleUrls: ['./active-interrogation-modal.component.css']
})
export class ActiveInterrogationModalComponent implements OnInit, OnDestroy {

  @ViewChild(PerviewCameraComponent, { static: false }) perviewCameraComponent: PerviewCameraComponent;

  @Output() onCloseModal = new EventEmitter<any>();
  @Input() activeInterrogation: ActiveInterrogation;
  @Input() disconnected: boolean;
  @Input() socket: any;

  public messages: ChatMessage[] = [];
  public message: string;
  public trigger: boolean = false;
  public showCamera: boolean = false;
  private modalClosed: boolean = false;

  constructor(private authService: AuthService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    $('#exampleModal').modal('show');

    this.socket.emit('connect-to-room', this.activeInterrogation.socketId);

    // for checking the time past from each message
    setInterval(() => {
      this.trigger = !this.trigger;
    }, 1000);

    // for the video camera plugin
    setTimeout(() => {
      this.showCamera = true;
      $(document).ready(() => {
        this.perviewCameraComponent.playVideo(this.activeInterrogation.camera, this.activeInterrogation.nvr);
      });
    }, 1000);
  }

  ngOnDestroy() {
    // if the user pressed on back button
    if (!this.modalClosed)
      this.closeModal();
  }

  public closeModal() {
    $('#exampleModal').modal('hide');
    this.modalClosed = true;

    if (!this.disconnected) {
      this.perviewCameraComponent.stopVideo();
      this.perviewCameraComponent.logout(this.activeInterrogation.nvr);
    }
    
    this.onCloseModal.emit();
  }

  public openSound() {
    this.perviewCameraComponent.openSound()
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.SUCCESS_OPEN_SOUND) {
          this.toasterService.showSuccess('Audio is on.');
        }
        else {
          this.toasterService.showError('Cannot turn on audio.');
        }
      });
  }

  public closeSound() {
    this.perviewCameraComponent.closeSound()
      .then((videoPreviewMessage: VideoPreviewMessage) => {
        if (videoPreviewMessage.status === VideoPerviewStatus.SUCCESS_CLOSE_SOUND) {
          this.toasterService.showSuccess('Audio is off.');
        }
        else {
          this.toasterService.showError('Cannot turn off audio.');
        }
      });
  }

  public addMessage(inOrOur: boolean, text?: string, userName?: string, userId?: number): number {
    this.scrollDown();
    this.trigger = !this.trigger;

    this.messages.push({
      class: inOrOur ? 'in' : 'out',
      imgSrc: inOrOur ? '../../../assets/images/avatar1.png' : '../../../assets/images/avatar6.png',
      userName: inOrOur ? userName : this.authService.getCurrentUserValue().userName,
      userId: inOrOur ? userId : this.authService.getCurrentUserValue().userId,
      text: inOrOur ? text : this.message,
      date: new Date().getTime(),
      error: false
    });

    return this.messages.length - 1;
  }

  public sendMessage() {

    this.scrollDown();
    this.trigger = !this.trigger;

    let insertedIndex = this.addMessage(false);

    // save the message
    let tempMessage = this.message;
    this.message = null;

    let chatMessage: ChatSocketMessage = {
      to: this.activeInterrogation.socketId,
      from: this.socket.id,
      userName: this.authService.getCurrentUserValue().userName,
      userId: this.authService.getCurrentUserValue().userId,
      text: tempMessage,
    };

    this.socket.emit('send-message', chatMessage, (err: string, data) => {
      if (err) {
        this.message = tempMessage;
        this.messages[insertedIndex].error = true;
      }
    });
  }

  private scrollDown() {
    $('.chat-list').animate({ scrollTop: $('.chat-list').prop('scrollHeight') }, 500);
  }
}
