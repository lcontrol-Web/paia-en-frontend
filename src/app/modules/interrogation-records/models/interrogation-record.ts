export class InterrogationRecord {
    public interrogationId: number;
    public nvrName: string;
    public cameraName: string;
    public investigatorNvrName: string;
    public investigatorCameraName: string;
    public interviewNumberEitan: number;
    public foreignName: string;
    public caseNumber: number;
    public foreignIdNumber: string;
    public nationality: string;
    public investigatorName: string;
    public userName: string;
    public startTime: Date;
    public endTime: Date;
    public burned: boolean;
    // 0 - not ready
    // 1 - send to archive
    // 2 - in archive
    public uploaded: number;
}
