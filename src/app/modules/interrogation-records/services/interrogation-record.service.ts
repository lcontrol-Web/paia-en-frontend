import { PaginationService } from './../../../core/services/pagination.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/core/services/data.service';
import { Interrogation } from '../../interrogations/models/interrogation';
import { InterrogationAddRoom } from '../../interrogations/models/Interrogation-add-room';
import { InterrogationRecord } from '../models/interrogation-record';

@Injectable({
  providedIn: 'root'
})
export class InterrogationRecordService {

  private apiName = 'interrogation-records';

  constructor(private dataService: DataService,
    private paginationService: PaginationService) { }

  public getInterrogationRecordsByRangeDates(rangeDates: Date[]) {
    this.dataService.apiName = this.apiName + '/range-dates';
    return this.dataService.getDataByObject(rangeDates);
  }

  public getInterrogationRecordsByRangeDatesAndPagination(rangeDates: Date[], pagination) {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName + '/range-dates';
    return this.dataService.getDataByObject(rangeDates, params);
  }

  public getAmountOfInterrogationRecordsByRangeDatesAndPagination(rangeDates: Date[], pagination) {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = this.apiName + '/amount/range-dates';
    return this.dataService.getDataByObject(rangeDates, params);
  }

  public getTextEditorAndChatMessagesByInterrogationId(interrogationId: number) {
    this.dataService.apiName = `${this.apiName}/${interrogationId}/text-editor-and-chat-messages`;
    return this.dataService.getData();
  }

  public getPauseContinueTimesByInterrogationId(interrogationId: number) {
    this.dataService.apiName = `${this.apiName}/${interrogationId}/pause-continue-times`;
    return this.dataService.getData();
  }

  public sendToArchive(interrogationRecord: InterrogationRecord): Observable<any> {
    this.dataService.apiName = this.apiName + '/send-to-archive';
    return this.dataService.update(interrogationRecord.interrogationId, interrogationRecord);
  }

  public sendToBurn(interrogationRecord: InterrogationRecord): Observable<any> {
    this.dataService.apiName = this.apiName + '/send-to-burn';
    return this.dataService.update(interrogationRecord.interrogationId, interrogationRecord);
  }

  public deleteInterrogation(interrogationId: number): Observable<any> {
    this.dataService.apiName = this.apiName + '/';
    return this.dataService.delete(interrogationId);
  }

  public ping(ip: string): Observable<any> {
    this.dataService.apiName = this.apiName + '/ping/' + ip;
    return this.dataService.getData();
  }

  public addRoom(interrogationAddRoom: InterrogationAddRoom): Observable<Interrogation> {
    this.dataService.apiName = this.apiName + '/add-room';
    return this.dataService.update(interrogationAddRoom.interrogationId, interrogationAddRoom);
  }

  public closeLiveInterrogation(socketId: string): Observable<any> {
    this.dataService.apiName = this.apiName + '/close-live-interrogation/' + socketId;
    return this.dataService.getData();
  }

}
