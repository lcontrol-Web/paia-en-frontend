import { environment } from './../../../../../environments/environment';
import { StreamService } from './../../../../core/services/stream.service';
import { ToasterService } from './../../../../core/services/toaster.service';
import { forkJoin } from 'rxjs';
import { Input, Output, EventEmitter, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ChatMessage } from 'src/app/core/models/chat-message';
import { AuthService } from 'src/app/core/services/auth.service';
import { InterrogationRecord } from '../../models/interrogation-record';
import { InterrogationRecordService } from '../../services/interrogation-record.service';
declare var $: any;

@Component({
  selector: 'app-interrogation-record-details-modal',
  templateUrl: './interrogation-record-details-modal.component.html',
  styleUrls: ['./interrogation-record-details-modal.component.css']
})
export class InterrogationRecordDetailsModalComponent implements OnInit, OnDestroy {

  @Input() interrogationRecord: InterrogationRecord;
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  @ViewChild('videoPlayer', { static: false }) videoplayer: ElementRef;

  public dataIsReady: boolean = false;
  public cannotReadFiles: boolean = false;
  public isLoading: boolean = false;
  public messages: ChatMessage[] = [];
  public editorText: string;
  public selectedFile: string;
  public streamUrl: string = '';
  public filesNames: string[] = [];

  constructor(private interrogationRecordService: InterrogationRecordService,
    private streamService: StreamService,
    private authService: AuthService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.getData();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#detailsModal').modal('show');
  }

  private closeModal() {
    $('#detailsModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private getData() {
    forkJoin([
      this.interrogationRecordService.getTextEditorAndChatMessagesByInterrogationId(this.interrogationRecord.interrogationId),
      this.streamService.getFilesInsideFolder(this.interrogationRecord.interrogationId)
    ])
      .subscribe(
        (response: any[]) => {
          this.messages = response[0].chatMessages ? JSON.parse(response[0].chatMessages) : '';
          this.editorText = response[0].editorText;
          this.filesNames = response[1];

          if (this.filesNames.length > 0) {
            this.selectedFile = this.filesNames[0];
            this.streamUrl = `${environment.apiUrl}/api/stream/?folder=${this.interrogationRecord.interrogationId}&file=${this.selectedFile}&token=${this.authService.getToken()}`;
          }

          this.dataIsReady = true;
        },
        (error: string) => {
          if (error === 'directoryNotExist')
            this.cannotReadFiles = true;
          else
            this.toasterService.showError('Cannot diplay the information.');
        });
  }

  public selectedFileChanged() {
    this.streamUrl = `${environment.apiUrl}/api/stream/?folder=${this.interrogationRecord.interrogationId}&file=${this.selectedFile}&token=${this.authService.getToken()}`;
  }

  public report() {
      
  }
}
