import { InterrogationRecordService } from './../../services/interrogation-record.service';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { InterrogationPauseContinueTimes } from 'src/app/modules/interrogations/models/Interrogation-pause-continue-times';
import { InterrogationService } from 'src/app/modules/interrogations/services/interrogation.service';
import { InterrogationRecord } from '../../models/interrogation-record';
declare var $: any;

@Component({
  selector: 'app-pause-continue-times-modal',
  templateUrl: './pause-continue-times-modal.component.html',
  styleUrls: ['./pause-continue-times-modal.component.css']
})
export class PauseContinueTimesModalComponent implements OnInit, OnDestroy {

  @Input() interrogationRecord: InterrogationRecord;
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public dataIsReady: boolean = false;
  public times: InterrogationPauseContinueTimes[] = [];

  constructor(private interrogationRecordService: InterrogationRecordService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.getData();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#pauseContinueTimesModal').modal('show');
  }

  private closeModal() {
    $('#pauseContinueTimesModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private getData() {
    this.interrogationRecordService.getPauseContinueTimesByInterrogationId(this.interrogationRecord.interrogationId)
      .subscribe(
        (times: any[]) => {
          this.times = times;
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot display information.');
        });
  }
}
