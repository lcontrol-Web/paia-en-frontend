import { InterrogationRecordService } from './../../services/interrogation-record.service';
import { ToasterService } from './../../../../core/services/toaster.service';
import { Component, EventEmitter, Input, Output, OnDestroy, OnInit } from '@angular/core';
import { InterrogationRecord } from '../../models/interrogation-record';
declare var $: any;

@Component({
  selector: 'app-send-to-burn-modal',
  templateUrl: './send-to-burn-modal.component.html',
  styleUrls: ['./send-to-burn-modal.component.css']
})
export class SendToBurnModalComponent implements OnInit, OnDestroy {

  @Input() interrogationRecord: InterrogationRecord;
  @Output('onConfirm') private confirmEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public dataIsReady: boolean = false;
  public isLoading: boolean = false;

  constructor(private interrogationRecordService: InterrogationRecordService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#sendToBurnModal').modal('show');
  }

  private closeModal() {
    $('#sendToBurnModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  public sendToBurn() {
    this.isLoading = true;
    this.interrogationRecordService.sendToBurn(this.interrogationRecord)
      .subscribe(
        (response: any) => {
          this.closeModal();
          setTimeout(() => {
            this.confirmEmitter.emit(true);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Cannot send record to burn.');
        });
  }
}
