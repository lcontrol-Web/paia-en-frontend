import { EventEmitter, Output, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { InterrogationService } from 'src/app/modules/interrogations/services/interrogation.service';
import { InterrogationRecord } from '../../models/interrogation-record';
declare var $: any;

@Component({
  selector: 'app-open-interrogations-modal',
  templateUrl: './open-interrogations-modal.component.html',
  styleUrls: ['./open-interrogations-modal.component.css']
})
export class OpenInterrogationsModalComponent implements OnInit, OnDestroy {

  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public dataIsReady: boolean = false;
  public openInterrogationRecords: InterrogationRecord[] = [];

  constructor(private interrogationService: InterrogationService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
    this.getData();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#openInterrogationsModal').modal('show');
  }

  private closeModal() {
    $('#openInterrogationsModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  private getData() {
    this.interrogationService.getOpenInterrogations()
      .subscribe(
        (openInterrogationRecors: InterrogationRecord[]) => {
          this.openInterrogationRecords = openInterrogationRecors;
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Cannot display the information');
        });
  }

  public deleteOpenInterrogationRecord(openInterrogationRecord: InterrogationRecord) {

    openInterrogationRecord['isLoading'] = true;
    this.interrogationService.deleteInterrogation(openInterrogationRecord.interrogationId)
      .subscribe(
        (interrogationId: string) => {
          let index = this.openInterrogationRecords.findIndex(function (o) {
            return o.interrogationId == +interrogationId;
          });

          if (index !== -1)
            this.openInterrogationRecords.splice(index, 1);

          this.dataIsReady = true;
        },
        (error: string) => {
          openInterrogationRecord['isLoading'] = false;
          this.toasterService.showError('Cannot delete interrogation');
        });
  }
}
