import { InterrogationRecordService } from './../../services/interrogation-record.service';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { InterrogationRecord } from '../../models/interrogation-record';
declare var $: any;

@Component({
  selector: 'app-send-to-archive-modal',
  templateUrl: './send-to-archive-modal.component.html',
  styleUrls: ['./send-to-archive-modal.component.css']
})
export class SendToArchiveModalComponent implements OnInit, OnDestroy {

  @Input() interrogationRecord: InterrogationRecord;
  @Output('onConfirm') private confirmEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public dataIsReady: boolean = false;
  public isLoading: boolean = false;

  constructor(private interrogationRecordService: InterrogationRecordService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#sendToArchiveModal').modal('show');
  }

  private closeModal() {
    $('#sendToArchiveModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  public sendToArchive() {
    this.isLoading = true;
    this.interrogationRecordService.sendToArchive(this.interrogationRecord)
      .subscribe(
        (response: any[]) => {
          this.toasterService.showSuccess('Successfully send record to archive.');
          this.closeModal();
          setTimeout(() => {
            this.confirmEmitter.emit(true);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Cannot send record to archive.');
        });
  }
}

