import { AuthService } from './../../core/services/auth.service';
import { LoggedUser } from './../../core/models/logged-user';
import { InterrogationRecordService } from './services/interrogation-record.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { Pagination } from 'src/app/core/models/pagination';
import { SortAction } from 'src/app/core/models/sort-action';
import { TableColumn } from 'src/app/core/models/table-column';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { InterrogationRecord } from './models/interrogation-record';

@Component({
  selector: 'app-interrogation-records',
  templateUrl: './interrogation-records.component.html',
  styleUrls: ['./interrogation-records.component.css']
})
export class InterrogationRecordsComponent implements OnInit {

  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public rangeDates: Date[] = [];
  public interrogationRecords: InterrogationRecord[] = [];
  public dataIsReady: boolean = false;
  public showDeleteForm: boolean = false;
  public showDetailsModal: boolean = false;
  public showPauseContinueTimesModal: boolean = false;
  public showOpenInterrogationsModal: boolean = false;
  public showSendToArchiveModal: boolean = false;
  public showSendToBurnModal: boolean = false;
  public selectedInterrogationRecord: InterrogationRecord;
  public itemsToDelete: InterrogationRecord[] = [];
  public checkedInterrogationRecords: InterrogationRecord[] = [];
  public loggedUser: LoggedUser;

  public columns: TableColumn[] = [
    {
      name: 'area',
      type: DataTypes.String,
      property: 'nvrName',
    },
    {
      name: 'room',
      type: DataTypes.String,
      property: 'cameraName',
    },
    // {
    //   name: 'investigator area',
    //   type: DataTypes.String,
    //   property: 'investigatorNvrName',
    // },
    // {
    //   name: 'investigator room',
    //   type: DataTypes.String,
    //   property: 'investigatorCameraName',
    // },
    {
      name: 'interview number',
      type: DataTypes.Number,
      property: 'interviewNumberEitan',
    },
    {
      name: 'foreign name',
      type: DataTypes.String,
      property: 'foreignName',
    },
    {
      name: 'case number',
      type: DataTypes.Number,
      property: 'caseNumber',
    },
    {
      name: 'foreign ID',
      type: DataTypes.Number,
      property: 'foreignIdNumber',
    },
    {
      name: 'nationality',
      type: DataTypes.String,
      property: 'nationality',
    },
    {
      name: 'investigator name',
      type: DataTypes.String,
      property: 'userName',
    },
  ];

  constructor(private interrogationRecordService: InterrogationRecordService,
    private authService: AuthService,
    private toasterService: ToasterService) {
    this.loggedUser = this.authService.getCurrentUserValue();
  }

  ngOnInit() {
    this.setDefaultTime();
    this.getInterrogationRecords();
  }

  private getInterrogationRecords() {
    forkJoin([
      this.interrogationRecordService.getInterrogationRecordsByRangeDatesAndPagination(this.rangeDates, this.pagination),
      this.interrogationRecordService.getAmountOfInterrogationRecordsByRangeDatesAndPagination(this.rangeDates, this.pagination),
    ])
      .subscribe(
        (response: [InterrogationRecord[], number]) => {
          this.interrogationRecords = response[0];
          this.total = response[1];
          this.dataIsReady = true;
        },
        (error: string) => {
          this.dataIsReady = true;
          this.toasterService.showError('Please try again later.');
        });
  }

  public onSearchByDatesClicked() {
    this.dataIsReady = false;
    this.rangeDates[0].setHours(0, 0, 0);
    this.rangeDates[1].setHours(23, 59, 59);
    this.getInterrogationRecords();
  }

  private setDefaultTime() {
    let d = new Date();
    d.setDate(d.getDate() - 8);
    this.rangeDates.push(d);

    let t = new Date();
    this.rangeDates.push(t);
  }

  public openDetailsModal(interrogation: InterrogationRecord) {
    this.selectedInterrogationRecord = interrogation;
    this.showDetailsModal = true;
  }

  public openPauseContinueTimesModal(interrogation: InterrogationRecord) {
    this.selectedInterrogationRecord = interrogation;
    this.showPauseContinueTimesModal = true;
  }

  public sendToArchive(interrogation: InterrogationRecord) {
    this.selectedInterrogationRecord = interrogation;
    this.showSendToArchiveModal = true;
  }

  public sentToBurn(interrogation: InterrogationRecord) {
    this.selectedInterrogationRecord = interrogation;
    this.showSendToBurnModal = true;
  }

  public deleteInterrogation(interrogation: InterrogationRecord) {
    this.itemsToDelete = [interrogation];
    this.showDeleteForm = true;
  }

  public closeDetailsModal() {
    this.showDetailsModal = false;
    this.selectedInterrogationRecord = null;
  }

  public closePauseContinueTimesModal() {
    this.showPauseContinueTimesModal = false;
    this.selectedInterrogationRecord = null;
  }

  public closeOpenInterogationsModal() {
    this.showOpenInterrogationsModal = false;
  }

  public onSendToArchive(value: boolean) {
    this.getInterrogationRecords();
    this.closeSendToArchiveModal();
  }

  public closeSendToArchiveModal() {
    this.showSendToArchiveModal = false;
  }

  public onSendToBurn(value: boolean) {
    this.getInterrogationRecords();
    this.closeSendToBurnModal();
  }

  public closeSendToBurnModal() {
    this.showSendToBurnModal = false;
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.pagination.page = page;
    this.getInterrogationRecords();
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.dataIsReady = false;
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;

    this.getInterrogationRecords();
  }


  public sortByColumn(sortAction: SortAction) {
    this.dataIsReady = false;
    this.pagination.sort = sortAction.sort;
    this.pagination.column = sortAction.column;
    this.getInterrogationRecords();
  }

  public filterByColumn(filterAction: FilterAction) {
    this.dataIsReady = false;
    this.pagination.condition = filterAction.condition;
    this.pagination.column = filterAction.column;
    this.pagination.searchText = filterAction.searchText;
    this.getInterrogationRecords();
  }

  public clearSortByColumn() {
    this.dataIsReady = false;
    this.pagination.sort = SortTypes.Descending;
    this.pagination.column = null;
    this.getInterrogationRecords();
  }

  public clearFilterByColumn() {
    this.dataIsReady = false;
    this.pagination.condition = null;
    this.pagination.searchText = null;
    this.getInterrogationRecords();
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public onSearch(searchValue: string) {
    this.pagination.page = 1;
    this.pagination.searchText = searchValue;
    this.pagination.condition = null;
    this.pagination.column = null;
    this.pagination.sort = SortTypes.Descending;
    this.getInterrogationRecords();
  }
}

