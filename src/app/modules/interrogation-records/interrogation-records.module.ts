import { OpenInterrogationsModalComponent } from './components/open-interrogations-modal/open-interrogations-modal.component';
import { SharedModule } from './../../shared/shared.module';
import { SendToBurnModalComponent } from './components/send-to-burn-modal/send-to-burn-modal.component';
import { SendToArchiveModalComponent } from './components/send-to-archive-modal/send-to-archive-modal.component';
import { PauseContinueTimesModalComponent } from './components/pause-continue-times-modal/pause-continue-times-modal.component';
import { InterrogationRecordDetailsModalComponent } from './components/interrogation-record-details-modal/interrogation-record-details-modal.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterrogationRecordsComponent } from './interrogation-records.component';
import { CalendarModule } from 'primeng/calendar';

const routes: Routes = [
  {
    path: '',
    component: InterrogationRecordsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CalendarModule,
    SharedModule
  ],
  declarations: [
    InterrogationRecordsComponent,
    InterrogationRecordDetailsModalComponent,
    OpenInterrogationsModalComponent,
    PauseContinueTimesModalComponent,
    SendToArchiveModalComponent,
    SendToBurnModalComponent
  ]
})
export class InterrogationRecordsModule { }
