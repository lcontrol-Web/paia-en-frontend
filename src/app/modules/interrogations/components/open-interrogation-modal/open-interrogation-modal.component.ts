import { EventEmitter, Output, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-open-interrogation-modal',
  templateUrl: './open-interrogation-modal.component.html',
  styleUrls: ['./open-interrogation-modal.component.css']
})
export class OpenInterrogationModalComponent implements OnInit, OnDestroy {

  @Output('onConfirm') private confirmEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#openInterrogationModal').modal('show');
  }

  private closeModal() {
    $('#openInterrogationModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  public confirm() {
    this.closeModal();
    setTimeout(() => {
      this.confirmEmitter.emit(true);
    }, 200);
  }
}
