import { ControllerService } from './../../services/controller.service';
import { ToasterService } from './../../../../core/services/toaster.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Camera } from 'src/app/modules/cameras/models/camera';
import { CameraService } from 'src/app/modules/cameras/services/camera.service';
import { Nvr } from 'src/app/modules/nvrs/models/nvr';
import { NvrService } from 'src/app/modules/nvrs/services/nvr.service';
import nationalities from '../../../../../assets/nationalities.json';
import { Interrogation } from '../../models/interrogation';
import { ChatIntterogationService } from '../../services/chat-intterogation.service';
import { InterrogationService } from '../../services/interrogation.service';
declare var $: any;

@Component({
  selector: 'app-interrogation-settings',
  templateUrl: './interrogation-settings.component.html',
  styleUrls: ['./interrogation-settings.component.css']
})
export class InterrogationSettingsComponent implements OnInit {

  @Output() onStartIntterogation = new EventEmitter<any>();

  public dataIsReady: boolean = false;
  public outputStatus: boolean = false;
  public outputLoading: boolean = false;
  public cannotGetOutputStatus: boolean = false;
  public nationalities: any[] = nationalities;
  public interrogation: Interrogation = new Interrogation();
  public openInterrogations: Interrogation[] = [];
  public openInterrogation: Interrogation = new Interrogation();
  public nvrs: Nvr[] = [];
  public cameras: Camera[] = [];
  public cameras1: Camera[] = [];
  public cameras2: Camera[] = [];
  public selectedNvr1: Nvr = null;
  public selectedNvr2: Nvr = null;
  public selectedCamera1: Camera = null;
  public selectedCamera2: Camera = null;
  public showOpenInterrogationModal: boolean = false;

  constructor(private nvrService: NvrService,
    private cameraService: CameraService,
    private interrogationService: InterrogationService,
    private controllerService: ControllerService,
    private toasterService: ToasterService,
    private chatIntterogationService: ChatIntterogationService) {
  }

  ngOnInit() {
    this.getData();
  }

  private getData() {
    forkJoin([
      this.nvrService.getNvrs(),
      this.interrogationService.getOpenInterrogations()
    ])
      .subscribe(
        (response: [Nvr[], Interrogation[]]) => {
          this.nvrs = response[0];
          this.openInterrogations = response[1];
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  private getCameras(nvrId: number, state?: number) {
    this.cameraService.getCamerasByNvrId(nvrId)
      .subscribe(
        (cameras: Camera[]) => {
          switch (state) {
            case 1:
              this.cameras1 = cameras;
              break;
            case 2:
              this.cameras2 = cameras;
              break;
            default:
              this.cameras1 = cameras;
              this.cameras2 = cameras;
              break;
          }
          this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  public nvr1Changed(nvr: Nvr) {
    this.dataIsReady = false;
    this.outputStatus = false;
    this.selectedCamera1 = null;
    this.getCameras(this.selectedNvr1.nvrId, 1);
  }

  public nvr2Changed(nvr: Nvr) {
    this.dataIsReady = false;
    this.selectedCamera2 = null;
    this.getCameras(this.selectedNvr2.nvrId, 2);
  }

  public camera1Changed() {
    this.checkOpenInterrogation();
    this.getOutputStatus();
  }

  public camera2Changed() {

  }

  public getOutputStatus() {
    this.outputLoading = true;
    this.outputStatus = null;
    this.cannotGetOutputStatus = false;
    this.controllerService.getOutputStatus(this.selectedCamera1.cameraId)
      .subscribe(
        (response: boolean) => {
          this.outputStatus = response;
          this.outputLoading = false;
        },
        (error: string) => {
          this.outputLoading = false;
          this.cannotGetOutputStatus = true;
          this.toasterService.showError('Cannot get the state of the controller.');
        });
  }

  public turnOffOutput() {
    // this.outputLoading = true;
    // this.controllerService.turnOnOffOutput(this.selectedCamera1.cameraId, { active: false })
    //   .subscribe(
    //     (response: boolean) => {
    //       this.outputStatus = response;
    //       this.outputLoading = false;
    //     },
    //     (error: string) => {
    //       this.outputStatus = false;
    //       this.outputLoading = false;
    //       this.toasterService.showError('Cannot turn off the output in the controller.');
    //     });
  }

  public continueInterrogation(value: boolean) {
    this.interrogation = this.openInterrogation;
    // this.interrogation.interrogationId = this.openInterrogation.interrogationId;
    // this.interrogation.editorText = this.openInterrogation.editorText;
    let messages = JSON.parse(<any>this.openInterrogation.chatMessages);
    this.chatIntterogationService.setMessages(JSON.parse(<any>this.openInterrogation.chatMessages));
    this.closeOpenInterrogationModal();
    this.onStartIntterogation.emit({
      camera: this.selectedCamera1,
      nvr: this.selectedNvr1,
      interrogation: this.interrogation
    });
  }

  public closeOpenInterrogationModal() {
    this.showOpenInterrogationModal = false;
  }

  public startInterrogation() {
    this.interrogation.nvrId = this.selectedNvr1.nvrId;
    this.interrogation.cameraId = this.selectedCamera1.cameraId;
    this.interrogation.investigatorNvrId = this.selectedNvr2.nvrId;
    this.interrogation.investigatorCameraId = this.selectedCamera2.cameraId;

    this.onStartIntterogation.emit({
      camera: this.selectedCamera1,
      nvr: this.selectedNvr1,
      interrogation: this.interrogation
    });
  }

  private checkOpenInterrogation() {
    for (const openInterrogation of this.openInterrogations) {
      if (this.selectedCamera1 && openInterrogation.cameraId === this.selectedCamera1.cameraId) {
        let d1 = new Date(openInterrogation.startTime);
        let d2 = new Date();
        if (this.sameDay(d1, d2)) {
          $(document).ready(() => {
            this.openInterrogation = JSON.parse(JSON.stringify(openInterrogation));
            this.showOpenInterrogationModal = true;
            return;
          });
        }
      }
    }
  }

  private sameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }
}
