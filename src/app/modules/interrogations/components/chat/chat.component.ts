import { ActiveInterrogation } from './../../models/active-interrogation';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { environment } from './../../../../../environments/environment';
import { io, Socket } from 'socket.io-client';
import { ChatMessage } from 'src/app/core/models/chat-message';
import { ChatSocketMessage } from 'src/app/core/models/chat-socket-message';
import { AuthService } from 'src/app/core/services/auth.service';
import { Camera } from 'src/app/modules/cameras/models/camera';
import { Nvr } from 'src/app/modules/nvrs/models/nvr';
import { Interrogation } from '../../models/interrogation';
import { ChatIntterogationService } from '../../services/chat-intterogation.service';
declare var $: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  @Input() interrogation: Interrogation;
  @Input() nvr: Nvr;
  @Input() camera: Camera;

  private socket: Socket;
  public message: string;
  public messages: ChatMessage[] = [];
  public sendTo: string;
  public trigger: boolean = false;
  public displayChat: boolean = false;
  private triggerInterval;

  constructor(private authService: AuthService,
    private chatIntterogationService: ChatIntterogationService) { }

  ngOnInit() {
    this.messages = this.chatIntterogationService.getMessages();
    this.setupSocketConnection();

    // for checking the time past from each message
    this.triggerInterval = setInterval(() => {
      this.trigger = !this.trigger;
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.triggerInterval);
    this.disconnect();
  }

  setupSocketConnection() {
    this.socket = io(environment.socketUrl, {
      query: {
        clientType: 2
      }
    });

    this.socket.on('connect', () => {
      console.log('Connected to socket');

      let activeInterrogation: ActiveInterrogation = {
        interrogation: this.interrogation,
        nvr: this.nvr,
        camera: this.camera,
        userId: this.authService.getCurrentUserValue().userId,
        userName: this.authService.getCurrentUserValue().userName,
        socketId: this.socket.id
      };

      // create a room for this interrogation
      this.socket.emit('create-room', activeInterrogation);
    });

    this.socket.on('message', (message: ChatSocketMessage) => {

      // check if this is not our socket send the message
      if (message.from != this.socket.id) {
        this.showChat();
        this.addMessage(true, message.text, message.userName);
      }
    });

    this.socket.on('connected-to-room', roomName => {
      console.log('You are connected to room: ' + roomName);
    });

    this.socket.on('connect_error', (err) => {
      console.log(err);
    });

    this.socket.on('error', (err) => {
      console.log(err);
    });

    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });
  }

  public showChat() {
    this.displayChat = true;
    setTimeout(() => {
      this.scrollDown();
    }, 50);
  }

  public hideChat() {
    this.displayChat = false;
  }

  public sendMessage() {

    let insertedIndex = this.addMessage(false);

    // save the message
    let tempMessage = this.message;
    this.message = null;

    let chatMessage: ChatSocketMessage = {
      to: this.socket.id,
      from: this.socket.id,
      userName: this.authService.getCurrentUserValue().userName,
      userId: this.authService.getCurrentUserValue().userId,
      text: tempMessage,
    };

    this.socket.emit('send-message', chatMessage, (err: string, data) => {

      if (err) {
        this.message = tempMessage;
        this.messages[insertedIndex].error = true;
        this.chatIntterogationService.setMessages(this.messages);
      }
    });
  }

  private disconnect() {
    if (this.socket)
      this.socket.disconnect();
  }

  private addMessage(inOrOur: boolean, text?: string, userName?: string, userId?: number): number {

    this.scrollDown();
    this.trigger = !this.trigger;

    this.messages.push({
      class: inOrOur ? 'in' : 'out',
      imgSrc: inOrOur ? '../../../assets/images/avatar1.png' : '../../../assets/images/avatar6.png',
      userName: inOrOur ? userName : this.authService.getCurrentUserValue().userName,
      userId: inOrOur ? userId : this.authService.getCurrentUserValue().userId,
      text: inOrOur ? text : this.message,
      date: new Date().getTime(),
      error: false
    });

    this.chatIntterogationService.setMessages(this.messages);
    return this.messages.length - 1;
  }

  private scrollDown() {
    $('.chat-list').animate({ scrollTop: $('.chat-list').prop('scrollHeight') }, 500);
  }
}

