import { EventEmitter, OnDestroy } from '@angular/core';
import { Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-exit-interrogation-modal',
  templateUrl: './exit-interrogation-modal.component.html',
  styleUrls: ['./exit-interrogation-modal.component.css']
})
export class ExitInterrogationModalComponent implements OnInit, OnDestroy {

  @Output('onExit') private exitEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#exitInnterogationModal').modal('show');
  }

  private closeModal() {
    $('#exitInnterogationModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  public confirm() {
    this.exitEmitter.emit(true);
  }
}
