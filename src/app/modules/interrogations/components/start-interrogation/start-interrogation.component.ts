import { ControllerService } from './../../services/controller.service';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { VideoPerviewStatus } from 'src/app/core/enums/video-perview-status.enum';
import { VideoPreviewMessage } from 'src/app/core/models/video-preview-message';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { Camera } from 'src/app/modules/cameras/models/camera';
import { Nvr } from 'src/app/modules/nvrs/models/nvr';
import { PerviewCameraComponent } from 'src/app/shared/components/perview-camera/perview-camera.component';
import { Interrogation } from '../../models/interrogation';
import { InterrogationService } from '../../services/interrogation.service';

declare var $: any;

@Component({
  selector: 'app-start-interrogation',
  templateUrl: './start-interrogation.component.html',
  styleUrls: ['./start-interrogation.component.css']
})
export class StartInterrogationComponent implements OnInit, OnDestroy {

  @ViewChild(PerviewCameraComponent, { static: true }) perviewCameraComponent: PerviewCameraComponent;
  @Output() onStopIntterogation = new EventEmitter<any>();
  @Input() nvr: Nvr;
  @Input() camera: Camera;
  @Input() interrogation: Interrogation;

  public waitingForCameraToStart: boolean = false;
  public interrogationStarted: boolean = false;
  public interrogationPaused: boolean = false;
  public outputError: boolean = false;

  public countUpTimerConfig;
  public selectedAmountOfScreens: number = 1;
  public selectedWindowNumber: number = 0;
  public outputLoading: boolean = false;
  public outputStatus: boolean = false;
  public continueInterrogationIsLoading: boolean = false;
  public showEndInterrogationModal: boolean = false;
  public showExitInterrogationModal: boolean = false;
  public showOpenInterrogationModal: boolean = false;
  public showPauseInterrogationModal: boolean = false;
  public showContinueInterrogationModal: boolean = false;
  public submitExistPressed: boolean = false;

  public display: string = '00:00:00';
  private time: number = 0;
  private elapsedTimeInterval;

  private isCameraConnectedInterval;
  private pingCameraInterval;

  constructor(private router: Router,
    private interrogationService: InterrogationService,
    private controllerService: ControllerService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.startInterrogation();
  }

  ngOnDestroy() {
    if (this.isCameraConnectedInterval)
      clearInterval(this.isCameraConnectedInterval);

    if (this.pingCameraInterval)
      clearInterval(this.pingCameraInterval);

    if (this.elapsedTimeInterval)
      clearInterval(this.elapsedTimeInterval);
  }

  public async startInterrogation() {
    this.waitingForCameraToStart = true;

    let outputIsOn: boolean = await this.turnOnOutput();

    if (!outputIsOn)
      this.stopInterrogation(true);
    else {
      let cameraIsOn: boolean = await this.pingCamera();
      if (!cameraIsOn)
        this.stopInterrogation(true);
      else
        this.playCamera();
    }
  }

  public onWindowChanged(windowNumber: number) {
    this.selectedWindowNumber = windowNumber;
  }

  public openEndIterrogationModal() {
    $('object').css('display', 'none');
    this.showEndInterrogationModal = true;
  }

  public openPauseInterrogationModal() {
    $('object').css('display', 'none');
    this.showPauseInterrogationModal = true;
  }

  public openContinueInterrogationModal() {
    $('object').css('display', 'none');
    this.showContinueInterrogationModal = true;
  }

  public onEndInterrogation(value: boolean) {
    this.stopInterrogation();
    this.closeEndInterrogationModal();
  }

  public closeEndInterrogationModal() {
    setTimeout(() => {
      $('object').css('display', 'block');
      this.showEndInterrogationModal = false;
    }, 200);
  }


  public pauseInterrogation(value: boolean) {
    this.interrogationPaused = true;
    this.perviewCameraComponent.logout(this.nvr);
    this.pauseTimer();
    // this.turnOffOutput();
    this.closeEndInterrogationModal();
  }

  public closePauseInterrogationModal() {
    setTimeout(() => {
      $('object').css('display', 'block');
      this.showPauseInterrogationModal = false;
    }, 400);
  }

  public continueInterrogation(value: boolean) {
    this.continueInterrogationIsLoading = true;
    this.startInterrogation();
    this.closeEndInterrogationModal();
  }

  public closeContinueInterrogationModal() {
    setTimeout(() => {
      $('object').css('display', 'block');
      this.showContinueInterrogationModal = false;
    }, 400);
  }


  // public onCloseExitInterrogationModal(confirm: boolean) {

  //   if (confirm) {
  //     this.submitExistPressed = true;
  //     this.router.navigate(['']);
  //   }
  //   else
  //     this.submitExistPressed = false;

  //   $('object').css('display', 'block');

  //   setTimeout(() => {
  //     this.showExitInterrogationModal = false;
  //   }, 500);
  // }

  private stopInterrogation(showErrorMsg: boolean = false) {
    this.waitingForCameraToStart = false;
    this.interrogationStarted = false;
    this.perviewCameraComponent.logout(this.nvr);
    this.interrogationPaused = false;
    this.continueInterrogationIsLoading = false;
    // this.turnOffOutput();

    this.onStopIntterogation.emit();

    if (showErrorMsg)
      this.showStartInterrogationErrorMessage();
  }

  public playCamera() {

    this.isCameraConnectedInterval = setInterval(() => {

      this.perviewCameraComponent.playVideo(this.camera, this.nvr)
        .then(async (videoPerviewMessage: VideoPreviewMessage) => {

          if (videoPerviewMessage.status === VideoPerviewStatus.NVR_LOGIN_FAILED && !this.submitExistPressed) {
            clearInterval(this.isCameraConnectedInterval);
            this.stopInterrogation();
          }
          else if (videoPerviewMessage.status === VideoPerviewStatus.SUCCESS_PLAYING_VIDEO && !this.submitExistPressed) {
            clearInterval(this.isCameraConnectedInterval);

            let startInterrogation = true;

            // if this is a new interrogation or we continued an old interrogation
            if (!this.interrogation.interrogationId)
              startInterrogation = await this.saveStartInterrogation();



            // if interrogation paused  
            if (this.interrogationPaused) {
              this.interrogationPaused = false;
              this.continueInterrogationIsLoading = false;
            }

            if (startInterrogation) {
              this.waitingForCameraToStart = false;
              this.interrogationStarted = true;
              this.startTimer();
            }
            else
              this.stopInterrogation(true);
          }
        })
        .catch((error) => {
          clearInterval(this.isCameraConnectedInterval);
          this.stopInterrogation(true);
        });
    }, 5000);
  }

  private saveStartInterrogation(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.interrogationService.startInterrogation(this.interrogation)
        .subscribe(
          (interrogation: Interrogation) => {
            if (interrogation) {
              this.interrogation.interrogationId = +interrogation.interrogationId;
              resolve(true);
            }
            else
              resolve(false);
          },
          (error: string) => {
            console.log(error);
            resolve(false);
          });
    });
  }

  private pingCamera(): Promise<boolean> {

    let numberOfRequests = 0;

    return new Promise((resolve, reject) => {
      this.pingCameraInterval = setInterval(() => {
        this.controllerService.ping(this.camera.cameraIp)
          .subscribe(
            (alive: boolean) => {
              console.log('PING: ' + alive);
              numberOfRequests++;

              if (alive) {
                clearInterval(this.pingCameraInterval);
                resolve(true);
              }
              else if (numberOfRequests === 60) {
                console.log('Reached to maximum ping requests');
                clearInterval(this.pingCameraInterval);
                resolve(false);
              }
            },
            (err) => {
              clearInterval(this.pingCameraInterval);
              resolve(false);
            });
      }, 2000);
    });
  }

  public turnOnOutput(): Promise<boolean> {
    return new Promise((resolve) => {
      this.outputLoading = true;
      this.outputError = false;
      this.controllerService.turnOnOffOutput(this.camera.cameraId, { active: true })
        .subscribe(
          (response: boolean) => {
            this.outputStatus = response;
            this.outputLoading = false;
            resolve(response);
          },
          (error: string) => {
            this.toasterService.showError('Cannot turn on the controller.');
            this.outputLoading = false;
            this.outputError = true;
            resolve(false);
          });
    });
  }

  public turnOffOutput(): Promise<boolean> {
    return new Promise((resolve) => {
      this.outputLoading = true;
      this.outputError = false;
      this.controllerService.turnOnOffOutput(this.camera.cameraId, { active: false })
        .subscribe(
          (response: boolean) => {
            this.outputStatus = response;
            this.outputLoading = false;
            resolve(response);
          },
          (error: string) => {
            this.toasterService.showError('Cannot turn off the controller.');
            this.outputLoading = false;
            this.outputError = true;
            resolve(false);
          });
    });
  }

  private showStartInterrogationErrorMessage() {
    this.toasterService.showError('Cannot start the interrogation.');
  }

  private startTimer() {
    this.elapsedTimeInterval = setInterval(() => {
      if (this.time === 0)
        this.time++;
      else
        this.time++;

      this.display = this.transform(this.time);
    }, 1000);
  }

  private transform(value: number): string {

    let sec_num = value;
    let hours = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);

    let trueHours;
    let trueMinutes;
    let trueSeconds;

    if (hours < 10)
      trueHours = '0' + hours.toString();
    else
      trueHours = hours.toString();

    if (minutes < 10)
      trueMinutes = '0' + minutes.toString();
    else
      trueMinutes = minutes.toString();

    if (seconds < 10)
      trueSeconds = '0' + seconds.toString();
    else
      trueSeconds = seconds.toString();

    return trueHours + ':' + trueMinutes + ':' + trueSeconds;
  }

  pauseTimer() {
    clearInterval(this.elapsedTimeInterval);
  }

}
