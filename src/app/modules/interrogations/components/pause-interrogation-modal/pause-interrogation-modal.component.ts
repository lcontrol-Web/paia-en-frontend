import { InterrogationPause } from './../../models/interrogation-pause';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { Interrogation } from '../../models/interrogation';
import { ChatIntterogationService } from '../../services/chat-intterogation.service';
import { InterrogationService } from '../../services/interrogation.service';
declare var $: any;

@Component({
  selector: 'app-pause-interrogation-modal',
  templateUrl: './pause-interrogation-modal.component.html',
  styleUrls: ['./pause-interrogation-modal.component.css']
})
export class PauseInterrogationModalComponent implements OnInit, OnDestroy {

  @Input() interrogation: Interrogation;
  @Output('onPause') private pauseEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public isLoading: boolean = false;

  constructor(private toasterService: ToasterService,
    private interrogationService: InterrogationService,
    private chatIntterogationService: ChatIntterogationService) { }

    ngOnInit() {
      this.openModal();
    }
  
    ngOnDestroy() {
      this.closeModal();
    }
  
    private openModal() {
      $('#pauseInterrogationModal').modal('show');
    }
  
    private closeModal() {
      $('#pauseInterrogationModal').modal('hide');
    }
  
    public onCloseClicked() {
      this.closeModal();
      setTimeout(() => {
        this.closeEmitter.emit();
      }, 200);
    }
  

  public endInterrigation() {
    this.isLoading = true;

    let interrogationPause: InterrogationPause = {
      interrogationId: this.interrogation.interrogationId,
      editorText: this.interrogation.editorText,
      chatMessages: this.chatIntterogationService.getMessages().length > 0 ? JSON.stringify(this.chatIntterogationService.getMessages()) : null
    };

    this.interrogationService.pauseInterrogation(interrogationPause)
      .subscribe(
        (response: string) => {
          this.toasterService.showSuccess('Successfully paused the interrogation');

          // // clean all the fields
          // this.interrogation.editorText = null;
          // this.interrogation.interrogationId = null;
          // this.chatIntterogationService.clearMessages();
          this.closeModal();
          setTimeout(() => {
            this.pauseEmitter.emit(true);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Cannot pause the interrogation.');
        });
  }

}
