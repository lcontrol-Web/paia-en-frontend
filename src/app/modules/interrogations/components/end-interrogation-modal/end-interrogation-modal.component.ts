import { ToasterService } from './../../../../core/services/toaster.service';
import { Component, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Interrogation } from '../../models/interrogation';
import { InterrogationEnd } from '../../models/interrogation-end';
import { InterrogationService } from '../../services/interrogation.service';
import { ChatIntterogationService } from '../../services/chat-intterogation.service';
declare var $: any;

@Component({
  selector: 'app-end-interrogation-modal',
  templateUrl: './end-interrogation-modal.component.html',
  styleUrls: ['./end-interrogation-modal.component.css']
})
export class EndInterrogationModalComponent implements OnInit, OnDestroy {

  @Input() interrogation: Interrogation;
  @Output('onEnd') private endEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public isLoading: boolean = false;

  constructor(private toasterService: ToasterService,
    private interrogationService: InterrogationService,
    private chatIntterogationService: ChatIntterogationService) { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#endInterrogationModal').modal('show');
  }

  private closeModal() {
    $('#endInterrogationModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }


  public endInterrigation() {
    this.isLoading = true;

    let interrogationEnd: InterrogationEnd = {
      interrogationId: this.interrogation.interrogationId,
      editorText: this.interrogation.editorText,
      chatMessages: this.chatIntterogationService.getMessages().length > 0 ? JSON.stringify(this.chatIntterogationService.getMessages()) : null
    };

    this.interrogationService.endInterrogation(interrogationEnd)
      .subscribe(
        (response: string) => {
          this.toasterService.showSuccess('Successfully saved interogation.');
          // clean all the fields
          this.interrogation.editorText = null;
          this.interrogation.interrogationId = null;
          this.chatIntterogationService.clearMessages();
          this.closeModal();
          setTimeout(() => {
            this.endEmitter.emit(true);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Error saving interrogation.');
        });
  }

}
