import { ToasterService } from './../../../../core/services/toaster.service';
import { Input, Output, OnDestroy } from '@angular/core';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { Interrogation } from '../../models/interrogation';
import { InterrogationService } from '../../services/interrogation.service';
declare var $: any;

@Component({
  selector: 'app-continue-interrogation-modal',
  templateUrl: './continue-interrogation-modal.component.html',
  styleUrls: ['./continue-interrogation-modal.component.css']
})
export class ContinueInterrogationModalComponent implements OnInit, OnDestroy {

  @Input() interrogation: Interrogation;
  @Output('onContinue') private continueEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output('onClose') private closeEmitter: EventEmitter<null> = new EventEmitter();

  public isLoading: boolean = false;

  constructor(private toasterService: ToasterService,
    private interrogationService: InterrogationService) { }

  ngOnInit() {
    this.openModal();
  }

  ngOnDestroy() {
    this.closeModal();
  }

  private openModal() {
    $('#continueInterrogationModal').modal('show');
  }

  private closeModal() {
    $('#continueInterrogationModal').modal('hide');
  }

  public onCloseClicked() {
    this.closeModal();
    setTimeout(() => {
      this.closeEmitter.emit();
    }, 200);
  }

  public continueInterrigation() {
    this.isLoading = true;
    this.interrogationService.continueInterrogation(this.interrogation.interrogationId)
      .subscribe(
        (response: any) => {
          this.closeModal();
          setTimeout(() => {
            this.continueEmitter.emit(true);
          }, 200);
        },
        (error: string) => {
          this.isLoading = false;
          this.toasterService.showError('Cannot continue interrogation.');
        });
  }

}