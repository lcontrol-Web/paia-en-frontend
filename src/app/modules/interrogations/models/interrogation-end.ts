export class InterrogationEnd {
    public interrogationId: number;
    public editorText: string;
    public chatMessages: string;
    public endTime?: Date;
}
