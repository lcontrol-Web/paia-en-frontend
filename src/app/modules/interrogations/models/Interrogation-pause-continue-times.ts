export class InterrogationPauseContinueTimes {
    public pauseInterrogationId: number;
    public interrogationId: number;
    public pauseTime: Date;
    public continueTime: Date;
}
