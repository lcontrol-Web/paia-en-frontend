import { ChatMessage } from '../../../core/models/chat-message';

export class Interrogation {
    public interrogationId?: number;
    public nvrId: number;
    public cameraId: number;
    public investigatorNvrId: number;
    public investigatorCameraId: number;
    public foreignName: string = 'person';
    public caseNumber: number = 1234;
    public interviewNumberEitan: number = 1234;
    public foreignIdNumber: string = '1234';
    public nationality: string = null;
    public editorText?: string;
    public chatMessages?: ChatMessage[];
    public userId?: number;
    public startTime?: Date;
    public endTime?: Date;
}

export class Comment {
    public text: string;
    public date: number;
}