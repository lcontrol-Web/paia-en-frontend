import { Camera } from '../../cameras/models/camera';
import { Nvr } from '../../nvrs/models/nvr';
import { Interrogation } from './interrogation';

export class ActiveInterrogation {
    public interrogation: Interrogation;
    public nvr: Nvr;
    public camera: Camera;
    public socketId: string;
    public userId: number;
    public userName: string;
}
