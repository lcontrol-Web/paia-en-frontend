export class InterrogationPause {
    public interrogationId: number;
    public editorText: string;
    public chatMessages: string;
}
