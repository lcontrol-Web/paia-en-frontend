import { CanDeactivateInterrogationGuard } from './../../core/guards/interrogation.guard';
import { ChatComponent } from './components/chat/chat.component';
import { StartInterrogationComponent } from './components/start-interrogation/start-interrogation.component';
import { PauseInterrogationModalComponent } from './components/pause-interrogation-modal/pause-interrogation-modal.component';
import { OpenInterrogationModalComponent } from './components/open-interrogation-modal/open-interrogation-modal.component';
import { InterrogationSettingsComponent } from './components/interrogation-settings/interrogation-settings.component';
import { ExitInterrogationModalComponent } from './components/exit-interrogation-modal/exit-interrogation-modal.component';
import { EndInterrogationModalComponent } from './components/end-interrogation-modal/end-interrogation-modal.component';
import { ContinueInterrogationModalComponent } from './components/continue-interrogation-modal/continue-interrogation-modal.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { InterrogationsComponent } from './interrogations.component';
import { EditorModule } from 'primeng/editor';

const routes: Routes = [
  {
    path: '',
    canDeactivate: [CanDeactivateInterrogationGuard],
    component: InterrogationsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    EditorModule
  ],
  declarations: [
    InterrogationsComponent,
    ContinueInterrogationModalComponent,
    EndInterrogationModalComponent,
    ExitInterrogationModalComponent,
    InterrogationSettingsComponent,
    OpenInterrogationModalComponent,
    PauseInterrogationModalComponent,
    StartInterrogationComponent,
    ChatComponent
  ]
})
export class InterrogationsModule { }
