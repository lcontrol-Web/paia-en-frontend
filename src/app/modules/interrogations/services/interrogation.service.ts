import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/core/services/data.service';
import { Camera } from '../../cameras/models/camera';
import { InterrogationRecord } from '../../interrogation-records/models/interrogation-record';
import { Interrogation } from '../models/interrogation';
import { InterrogationAddRoom } from '../models/Interrogation-add-room';
import { InterrogationEnd } from '../models/interrogation-end';

@Injectable({
  providedIn: 'root'
})
export class InterrogationService {

  private apiName = 'interrogations';

  constructor(private dataService: DataService) { }

  public getOpenInterrogations() {
    this.dataService.apiName = `${this.apiName}/open`;
    return this.dataService.getData();
  }

  public startInterrogation(interrogation: Interrogation): Observable<Interrogation> {
    this.dataService.apiName = this.apiName + '/start';
    return this.dataService.add(interrogation);
  }

  public endInterrogation(interrogationEnd: InterrogationEnd): Observable<any> {
    this.dataService.apiName = `${this.apiName}/end`;
    return this.dataService.update(interrogationEnd.interrogationId, interrogationEnd);
  }

  public pauseInterrogation(interrogationEnd: InterrogationEnd): Observable<any> {
    this.dataService.apiName = `${this.apiName}/pause`;
    return this.dataService.update(interrogationEnd.interrogationId, interrogationEnd);
  }

  public continueInterrogation(interrogationId: number): Observable<any> {
    this.dataService.apiName = `${this.apiName}/continue`;
    return this.dataService.update(interrogationId, {});
  }

  public deleteInterrogation(interrogationId: number): Observable<any> {
    this.dataService.apiName = this.apiName;
    return this.dataService.delete(interrogationId);
  }

  public addRoom(interrogationAddRoom: InterrogationAddRoom): Observable<Interrogation> {
    this.dataService.apiName = this.apiName + '/add-room';
    return this.dataService.update(interrogationAddRoom.interrogationId, interrogationAddRoom);
  }

  public closeActiveInterrogation(socketId: string): Observable<any> {
    this.dataService.apiName = this.apiName + '/close-active-interrogation/' + socketId;
    return this.dataService.getData();
  }
}
