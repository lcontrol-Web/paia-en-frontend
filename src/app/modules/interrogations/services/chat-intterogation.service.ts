import { Injectable } from '@angular/core';
import { ChatMessage } from 'src/app/core/models/chat-message';

@Injectable({
  providedIn: 'root'
})
export class ChatIntterogationService {

  private messages: ChatMessage[] = [];

  constructor() { }

  public getMessages() {
    return [...this.messages];
  }

  public setMessages(messages: ChatMessage[]) {
    if (messages)
      this.messages = [...messages];
  }

  public clearMessages() {
    this.messages = [];
  }

}
