import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/core/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class ControllerService {

  private apiName = 'controller';

  constructor(private dataService: DataService) { }

  public getOutputStatus(cameraId: number): Observable<boolean> {
    this.dataService.apiName = `${this.apiName}/${cameraId}/output-status`;
    return this.dataService.getData();
  }

  public turnOnOffOutput(cameraId: number, data: { active: boolean }): Observable<boolean> {
    this.dataService.apiName = `${this.apiName}/${cameraId}/turn-on-off-output`;
    return this.dataService.getDataByObject(data);
  }

  public ping(ip: string): Observable<any> {
    this.dataService.apiName = `${this.apiName}/${ip}/ping`;
    return this.dataService.getData();
  }
}
