import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Interrogation } from 'src/app/modules/interrogations/models/interrogation';
import { Camera } from '../cameras/models/camera';
import { Nvr } from '../nvrs/models/nvr';
declare var $: any;

@Component({
  selector: 'app-interrogations',
  templateUrl: './interrogations.component.html',
  styleUrls: ['./interrogations.component.css']
})
export class InterrogationsComponent implements OnInit {

  public startInterrogationPressed: boolean = false;
  public showExitInterrogationModal: boolean = false;
  public submitExistPressed: boolean = false;
  public selectedNvr: Nvr;
  public selectedCamera: Camera;
  public interrogation: Interrogation = new Interrogation();

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public startInterrogation(data: any) {
    this.startInterrogationPressed = true;
    this.selectedNvr = data.nvr;
    this.selectedCamera = data.camera;
    this.interrogation = data.interrogation;
  }

  public stopInterrogation() {
    this.startInterrogationPressed = false;
  }

  public exitInterrogationModal(value: boolean) {
    if (value) {
      this.submitExistPressed = true;
      this.router.navigate(['']);
    }
    else
      this.submitExistPressed = false;

    this.closeExitInterrogationModal();
  }

  public closeExitInterrogationModal() {
    $('object').css('display', 'block');
    setTimeout(() => {
      this.showExitInterrogationModal = false;
    }, 500);
  }

  public canExitComponent(): boolean {
    if (this.submitExistPressed || !this.startInterrogationPressed)
      return true;

    $('object').css('display', 'none');
    this.showExitInterrogationModal = true;
    return false;
  }

}

