import { PaginationService } from './../../../core/services/pagination.service';
import { DataService } from './../../../core/services/data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Log } from '../models/log';
import { Pagination } from 'src/app/core/models/pagination';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private apiName = 'logs';

  constructor(private dataService: DataService,
    private paginationService: PaginationService) { }

  public getLogs(): Observable<Log[]> {
    this.dataService.apiName = this.apiName;
    return this.dataService.getData();
  }

  public getLogsByRangeDates(rangeDates: Date[]): Observable<Log[]> {
    this.dataService.apiName = `${this.apiName}/range-dates`;
    return this.dataService.getDataByObject(rangeDates);
  }

  public getLogsByRangeDatesAndPagination(rangeDates: Date[], pagination: Pagination): Observable<Log[]> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = `${this.apiName}/range-dates`;
    return this.dataService.getDataByObject(rangeDates, params);
  }

  public getAmountOfLogsByRangeDatesAndPagination(rangeDates: Date[], pagination: Pagination): Observable<number> {
    const params = this.paginationService.getPaginationParams(pagination);
    this.dataService.apiName = `${this.apiName}/amount/range-dates`;
    return this.dataService.getDataByObject(rangeDates, params);
  }
}
