export class Log {
    public logId : number;
    public userId : number;
    public userName: string;
    public action : string;
    public time : Date;
}