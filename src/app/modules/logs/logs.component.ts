import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { DataTypes } from 'src/app/core/enums/data-types.enum';
import { SortTypes } from 'src/app/core/enums/sort-types.enum';
import { FilterAction } from 'src/app/core/models/filter-action';
import { Pagination } from 'src/app/core/models/pagination';
import { SortAction } from 'src/app/core/models/sort-action';
import { TableColumn } from 'src/app/core/models/table-column';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import { Log } from './models/log';
import { LogService } from './services/log.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  public pagination: Pagination = new Pagination();
  public total: number = 0;
  public logs: Log[] = [];
  public dataIsReady: boolean = false;
  public rangeDates: Date[] = [];

  public columns: TableColumn[] = [
    {
      name: 'user name',
      type: DataTypes.String,
      property: 'userName',
    },
    {
      name: 'action',
      type: DataTypes.String,
      property: 'action',
    },
  ];

  constructor(private logService: LogService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.setDefaultTime();
    this.getLogs();
  }

  private getLogs() {
    this.logService.getLogsByRangeDatesAndPagination(this.rangeDates, this.pagination)
      .subscribe(
        (logs: Log[]) => {
          this.logs = logs;
          this.total = this.logs.length;
         this.dataIsReady = true;
        },
        (error: string) => {
          this.toasterService.showError('Please try again later.');
        });
  }

  public onSearchByDatesClicked() {
    this.dataIsReady = false;
    this.rangeDates[0].setHours(0, 0, 0);
    this.rangeDates[1].setHours(23, 59, 59);
    this.getLogs();
  }

  private setDefaultTime() {
    let d = new Date();
    d.setDate(d.getDate() - 7);
    this.rangeDates.push(d);

    let t = new Date();
    this.rangeDates.push(t);
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.pagination.page = page;
    this.getLogs();
  }

  public itemsPerPageChanged(itemsPerPage: number) {
    this.dataIsReady = false;
    this.pagination.itemsPerPage = itemsPerPage;
    if (this.pagination.itemsPerPage >= 10)
      this.pagination.page = 1;

    this.getLogs();
  }


  public sortByColumn(sortAction: SortAction) {
    this.dataIsReady = false;
    this.pagination.sort = sortAction.sort;
    this.pagination.column = sortAction.column;
    this.getLogs();
  }

  public filterByColumn(filterAction: FilterAction) {
    this.dataIsReady = false;
    this.pagination.condition = filterAction.condition;
    this.pagination.column = filterAction.column;
    this.pagination.searchText = filterAction.searchText;
    this.getLogs();
  }

  public clearSortByColumn() {
    this.dataIsReady = false;
    this.pagination.sort = SortTypes.Descending;
    this.pagination.column = null;
    this.getLogs();
  }

  public clearFilterByColumn() {
    this.dataIsReady = false;
    this.pagination.condition = null;
    this.pagination.searchText = null;
    this.getLogs();
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public onSearch(searchValue: string) {
    this.pagination.page = 1;
    this.pagination.searchText = searchValue;
    this.pagination.condition = null;
    this.pagination.column = null;
    this.pagination.sort = SortTypes.Descending;
    this.getLogs();
  }
}
