import { CalendarModule } from 'primeng/calendar';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { LogsComponent } from './logs.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: LogsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CalendarModule,
    SharedModule
  ],
  declarations: [LogsComponent]
})
export class LogsModule { }
